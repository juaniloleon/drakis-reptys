﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys.monitors.crud
{
    public partial class pointsView : Page
    {
        private int idUser;
        private bool isMonitor;
        private Monitor mon;
        private TipoAcceso priv;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["existeSesion"] == null || Session["isMonitor"] == null || Session["idUser"] == null)
            {
                Response.Redirect("/error/errorPage.aspx");
            }
            else
            {
                bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
                int.TryParse(Session["idUser"].ToString(), out idUser);
                mon = LNyAD.ObtenMonitor(idUser);
                priv = LNyAD.ObtenPrivilegios(mon.TipoAccesoID);
                if (!isMonitor || priv.NivelAcceso < 10)
                {
                    Response.Redirect("/error/errorPage.aspx");
                }
                else
                {
                    if (IsPostBack)
                    {
                        CargarDatos();
                    }
                    else
                    {
                        lbAvisoVacio.Visible = false;

                        CargarDDL();
                        CargarDatos();
                    }
                }
            }
        }

        private void AdaptarGV()
        {
            gvDatos.HeaderRow.Cells[1].Visible = false;
            gvDatos.HeaderRow.Cells[2].Text = "MONITOR";
            gvDatos.HeaderRow.Cells[3].Text = "SOCIOS";

            foreach (GridViewRow r in gvDatos.Rows)
            {
                r.Cells[1].Visible = false;
                r.Cells[2].Text = LNyAD.ObtenMonitor(r.Cells[2].Text).Nombre;
                r.Cells[3].Text = LNyAD.ObtenerSocio(r.Cells[3].Text).Nombre;
                r.Cells[5].Text =LNyAD.FormatoEuropeoFecha(r.Cells[5].Text);
            }
        }

        private void CargarDatos()
        {
            gvDatos.DataSource = LNyAD.Puntos();
            gvDatos.DataBind();
            if (gvDatos.Rows.Count > 0)
                AdaptarGV();
            else
                lbAvisoVacio.Visible = true;
        }

        private void CargarDDL()
        {
            ddlMonitors.Items.Add("SELECCIONE MONITOR");
            ddlSoc.Items.Add("SELECCIONE SOCIO");

            var mon = LNyAD.TodosLosMonitoresActivos();
            var soc = LNyAD.TodosLosSociossActivos();

            foreach (var s in mon)
                ddlMonitors.Items.Add(s);
            foreach (var s in soc)
                ddlSoc.Items.Add(s);

            ddlMonitors.SelectedIndex = 0;
            ddlSoc.SelectedIndex = 0;
        }

        protected void cvSoc_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ddlSoc.SelectedIndex == 0)
                args.IsValid = false;
        }

        protected void cvMon_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ddlMonitors.SelectedIndex == 0)
                args.IsValid = false;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Validate();
            if (Page.IsValid)
            {
                var user = ddlMonitors.SelectedValue.Substring(1, ddlMonitors.SelectedValue.IndexOf(')') - 1);

                var monID = LNyAD.MonitorPorUser(user);
                var socio = ddlSoc.SelectedValue.Substring(1, ddlSoc.SelectedValue.IndexOf(')') - 1);

                var socID = LNyAD.SocioPorUser(socio);

                int.TryParse(tbPuntos.Text, out var cantidad);

                lbHecho.Visible = true;
                lbHecho.Text = "Añadido";
                LNyAD.ActualizarAnyadirPuntos(new Puntos(-1, monID, socID, cantidad, DateTime.Now));
                Response.Redirect("pointsView.aspx");
            }
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("/monitors/panel.aspx");
        }

        protected void gvDatos_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            LNyAD.BorrarPuntos(gvDatos.Rows[e.RowIndex].Cells[1].Text);
            lbHecho.Visible = true;
            lbHecho.Text = "Borrado";
            Response.Redirect("pointsView.aspx");
        }
    }
}