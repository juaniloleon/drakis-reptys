﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using DrakisReptys.Logic.ReptysDataSetTableAdapters;
using DrakisReptys.Models;
using System.Web.Security;

namespace DrakisReptys.Logic
{
    public class LNyAD
    {
        private static readonly MonitoresTableAdapter monitoresTable = new MonitoresTableAdapter();
        private static ReptysDataSet.MonitoresDataTable monitoresRows = new ReptysDataSet.MonitoresDataTable();

        private static readonly TipoAccesoTableAdapter tipoAccesoTable = new TipoAccesoTableAdapter();
        private static ReptysDataSet.TipoAccesoDataTable tipoAccesoRows = new ReptysDataSet.TipoAccesoDataTable();

        private static readonly NivelesTableAdapter nivelesTable = new NivelesTableAdapter();
        private static ReptysDataSet.NivelesDataTable nivelesRows = new ReptysDataSet.NivelesDataTable();

        private static readonly ActividadesTableAdapter actividadesTable = new ActividadesTableAdapter();
        private static ReptysDataSet.ActividadesDataTable actividadesRows = new ReptysDataSet.ActividadesDataTable();

        private static readonly Mon_NivTableAdapter mon_NivTable = new Mon_NivTableAdapter();
        private static ReptysDataSet.Mon_NivDataTable mon_NivRows = new ReptysDataSet.Mon_NivDataTable();

        private static readonly SociosTableAdapter sociosTable = new SociosTableAdapter();
        private static ReptysDataSet.SociosDataTable sociosRows = new ReptysDataSet.SociosDataTable();

        private static readonly MensajesTableAdapter mensajesTable = new MensajesTableAdapter();
        private static ReptysDataSet.MensajesDataTable mensajesRows = new ReptysDataSet.MensajesDataTable();

        private static readonly PuntosTableAdapter puntosTableAdapter = new PuntosTableAdapter();
        private static ReptysDataSet.PuntosDataTable puntosRows = new ReptysDataSet.PuntosDataTable();


        //Existencias
        internal static bool ExisteLink(int idMon, int idLvl)
        {
            mon_NivRows = mon_NivTable.ExisteLink(idMon, idLvl);

            if (mon_NivRows.Rows.Count > 0)
                return true;
            return false;
        }

        internal static int? ExisteNombreNivel(string text)
        {
            return nivelesTable.NivelPorNombre(text);
        }

        internal static bool ExisteActividad(int idActivity, string text)
        {
            var id = actividadesTable.PorNombre(text);
            if (id != null)
                if (id != idActivity)
                    return true;
            return false;
        }


        //consultas
        internal static object RelMonNivel()
        {
            return mon_NivTable.PorNivel();
        }

        internal static object RelMonitorNiv()
        {
            return mon_NivTable.PorMonitor();
        }

        internal static object Actividades()
        {
            return actividadesTable.GetData();
        }

        internal static object Socios()
        {
            return sociosTable.GetData();
        }

        internal static object Niveles()
        {
            return nivelesTable.GetData();
        }

        internal static object NivelesEstado(int estado)
        {
            return nivelesTable.PorEstado(estado);
        }

        internal static object Monitores(int idTipo)
        {
            if (idTipo == 0)
                return monitoresTable.GetData();
            return monitoresTable.PorTipo(idTipo);
        }

        internal static object Puntos()
        {
            return puntosTableAdapter.GetData();
        }

        internal static object MensajesAdmin(int idMonitor)
        {
            if (idMonitor == 0)
                return mensajesTable.GetData();
            return Mensajes(idMonitor, true);
        }

        internal static object Socios(int estado)
        {
            return sociosTable.Activos(estado);
        }

        internal static object Monitores()
        {
            return Monitores(0);
        }

        internal static object MisSocios(int idUser)
        {
            return sociosTable.TSociosdelMonitor(idUser);
        }

        internal static object MisSocios(int idUser, int estado)
        {
            return sociosTable.TSociosdelMonitorEstado(idUser, estado);
        }


        internal static bool MonitorLibre(int idUser, string text)
        {
            var idEncontrado = monitoresTable.PorUsuario(text);

            if (idEncontrado == null || idEncontrado == idUser)
                return true;
            return false;
        }

        internal static bool MonitorMailLibre(int idUser, string text)
        {
            var idEncontrado = MonitorByMail(text);

            if (idEncontrado == idUser || idEncontrado == null)
                return true;
            return false;
        }

        //Cambio producido por borrado de constraint
        private static void CambiarPuntosSocios(int idMonitor)
        {
            var superAdmin = monitoresTable.SuperAdmin()[0].ID;

            if (superAdmin == idMonitor)
                superAdmin = monitoresTable.SuperAdmin()[1].ID;
            else
                puntosTableAdapter.CambiarMonitor(superAdmin, idMonitor);
        }

        private static void CambiarMonitorActividades(int idMonitor)
        {
            var superAdmin = monitoresTable.SuperAdmin()[0].ID;

            if (superAdmin == idMonitor)
                superAdmin = monitoresTable.SuperAdmin()[1].ID;
            else
                actividadesTable.CambiarMonitor(superAdmin, idMonitor);
        }

        private static void CambiarActividad(int idAct)
        {
            var idNuevaAct = actividadesTable.GetData()[0].ID;
            if (idNuevaAct == idAct)
                idNuevaAct = actividadesTable.GetData()[1].ID;


            sociosTable.ActualizarActividad1Borrada(idNuevaAct, idAct);
            sociosTable.ActualizarActividad2Borrada(idNuevaAct, idAct);
        }

        //Comprobación manual
        internal static bool FormaTelefono(string text)
        {
            int.TryParse(text, out var phone);

            if (phone <= 999999999 && phone >= 600000000)
                return true;
            return false;
        }

        //borrado

        internal static void BorrarActividad(string idActividad)
        {
            int.TryParse(idActividad, out var idAct);
            CambiarActividad(idAct);
            actividadesTable.DeleteQuery(idAct);
        }

        internal static void BorrarSocio(string text)
        {
            int.TryParse(text, out var idSocio);
            BorrarSocio(idSocio);
        }

        internal static void BorrarSocio(int idSocio)
        {
            sociosTable.Delete(idSocio);
        }

        internal static void BorrarMonitor(string idText)
        {
            int.TryParse(idText, out var idMonitor);

            BorrarMonitor(idMonitor);
        }

        internal static void BorrarMonitor(int idMonitor)
        {
            CambiarMonitorActividades(idMonitor);
            CambiarPuntosSocios(idMonitor);
            monitoresTable.DeleteQuery(idMonitor);
        }

        internal static void BorrarMensaje(string idText)
        {
            int.TryParse(idText, out var idMess);
            BorrarMensaje(idMess);
        }

        internal static void BorrarMensaje(int idMess)
        {
            mensajesTable.DeleteQuery(idMess);
        }

        internal static void BorrarNivel(string idText)
        {
            int.TryParse(idText, out var idNivel);

            nivelesTable.DeleteQuery(idNivel);
        }

        internal static void BorrarPuntos(string idPuntosText)
        {
            BorrarPuntos(Convert.ToInt32(idPuntosText));
        }

        internal static void BorrarPuntos(int idPuntos)
        {
            puntosTableAdapter.DeleteQuery(idPuntos);
        }

        internal static void BorrarLink(int idLink)
        {
            mon_NivTable.DeleteQuery(idLink);
        }

        internal static void BorrarLink(string idLink)
        {
            BorrarLink(Convert.ToInt32(idLink));
        }


        //Puntos y derivados
        internal static object MovimientoPorSocio(int idUsuario)
        {
            return puntosTableAdapter.MovimientosPorSocio(idUsuario);
        }

        internal static string TotalPuntos(string idUsuario)
        {
            int.TryParse(idUsuario, out var id);
            return TotalPuntos(id);
        }

        internal static string TotalPuntos(int idUsuario)
        {
            return puntosTableAdapter.TotalUsuario(idUsuario).ToString();
        }

        //Para Cumpleaños
        internal static List<string> Cumpleanyos()
        {
            var cumples = new List<string>();
            sociosRows = sociosTable.Cumpleanyos();

            if (sociosRows.Count > 0)
                foreach (var r in sociosRows)
                {
                    var culture = new CultureInfo("es-ES");
                    var fecha = r.FechaNacimiento.Date.ToString(culture).Split(' ')[0];
                    cumples.Add(r.Nombre + " -> el " + fecha + CuantoQueda(r.FechaNacimiento));
                }
            else
                cumples.Add("No hay cumpleaños en los próximos 30 días");


            return cumples;
        }

        internal static string FormatoEuropeoFecha(string inDate)
        {
            var culture = new CultureInfo("es-ES");
            DateTime.TryParse(inDate, out var fecha);
                

            return fecha.ToString(culture).Split(' ')[0]; ;
        }

        private static string CuantoQueda(DateTime fechaNacimiento)
        {
            var hoy = DateTime.Today;
            fechaNacimiento = fechaNacimiento.AddYears(-(fechaNacimiento.Year - 1));
            fechaNacimiento = fechaNacimiento.AddYears(+(DateTime.Today.Year - 1));

            var ts = fechaNacimiento - hoy;

            if (ts.Days == 0)
                return "<b> HOY</b>";
            return " en " + ts.Days + " días";
        }


        //Mensajería
        internal static void EnviarMensajeANivel(int idUser, int idNivel, string text)
        {
            var m = new Mensaje(-1, idUser, 0, text, DateTime.Now);
            var destinatarios = new List<int>();
            if (idNivel == 0) //TODOS MIS NIVELES
            {
                List<int> misNiveles;
                var acceso = ObtenPrivilegios(ObtenMonitor(idUser).TipoAccesoID).NivelAcceso;
                misNiveles = acceso < 8 ? MisNivelesActivosINT(idUser) : TodosLosNivelesID();
                
                foreach (var n in misNiveles)
                    destinatarios.AddRange(SociosDelNivel(n));
            }
            else
            {
                destinatarios = SociosDelNivel(idNivel);
            }

            foreach (var id in destinatarios)
            {
                m.SocID = id;
                ActualizarAnyadir(m);
            }
        }

        internal static object Mensajes(int idUsuario, bool isMonitor)
        {
            return isMonitor
                ? mensajesTable.MisMensajesEnviados(idUsuario)
                : mensajesTable.MisMensajesRecibidos(idUsuario);
        }


        //Socios
        internal static object SociosPorNivelActivos(int idNivel)
        {
            return idNivel == 0 ? sociosTable.Activos(1) : sociosTable.PorNivel(idNivel);
        }

        internal static object SociosPorNivelTodos(int idNivel)
        {
            return idNivel == 0 ? sociosTable.GetData() : sociosTable.PorNivelTodos(idNivel);
        }

        internal static object SociosPorNivelTodos(int idNivel, int estado)
        {
            if (idNivel == 0)
                return sociosTable.Activos(estado);

            return sociosTable.PorNivelyEstado(idNivel, estado);
        }


        //Lista de nombres

        internal static List<string> TodosLosNiveles() //todos los niveles activos
        {
            var lista = new List<string>();

            nivelesRows = nivelesTable.NivelesActivos();

            foreach (ReptysDataSet.NivelesRow r in nivelesRows.Rows) lista.Add(r.Nombre);


            return lista;
        }

        internal static List<string> MisNivelesActivos(int idUser)
        {
            var lista = new List<string>();

            nivelesRows = nivelesTable.NivelesDeMonitor(idUser);

            foreach (ReptysDataSet.NivelesRow r in nivelesRows.Rows) lista.Add(r.Nombre);

            return lista;
        }


        //obtiene listas de IDs
        private static List<int> MisNivelesActivosINT(int idUser)
        {
            var nivelesID = new List<int>();

            mon_NivRows = mon_NivTable.NivelesDe(idUser);

            foreach (var r in mon_NivRows) nivelesID.Add(r.NivID);


            return nivelesID;
        }

        private static List<int> SociosDelNivel(int idNivel)
        {
            var sociosID = new List<int>();

            sociosRows = sociosTable.PorNivel(idNivel);

            foreach (var r in sociosRows) sociosID.Add(r.ID);


            return sociosID;
        }

        private static List<int> TodosLosNivelesID()
        {
            var lista = new List<int>();

            nivelesRows = nivelesTable.NivelesActivos();

            foreach (ReptysDataSet.NivelesRow r in nivelesRows.Rows) lista.Add(r.ID);


            return lista;
        }


        //Rankings
        internal static object RankingTotal()
        {
            return puntosTableAdapter.RankingTotal();
        }

        internal static object RankingNivel(int lvl)
        {
            return puntosTableAdapter.RankingNivel(lvl);
        }


        //Obtiene un nombre por el ID del elemento
        internal static string NombreNivel(string v)
        {
            int.TryParse(v, out var id);
            return NombreNivel(id);
        }

        internal static string NombreNivel(int nivelID)
        {
            return ObtenNivel(nivelID).Nombre;
            
        }

        internal static string NombreActividad(string actIDT)
        {
            int.TryParse(actIDT, out var actID);
            return NombreActividad(actID);
        }

        internal static string NombreActividad(int actID)
        {
            return actividadesTable.NombreActividad(actID);
        }

        internal static string NombreAcceso(string tipoAccesoIDT)
        {
            int.TryParse(tipoAccesoIDT, out var tipoAccesoID);

            return NombreAcceso(tipoAccesoID);
        }

        internal static string NombreAcceso(int tipoAccesoID)
        {
            return tipoAccesoTable.PorID(tipoAccesoID)[0].Nombre;
        }

        internal static string NombreConUser(int monitorID)
        {
            var mon = ObtenMonitor(monitorID);


            return "(" + mon.Usuario + ") " + mon.Nombre;
        }


        //Obtiene elementos por algun campo
        internal static int NivelPorNombre(string selectedValue, bool advance)
        {
            if (advance && selectedValue.Equals("TODOS MIS NIVELES"))
                return 0;
            return NivelPorNombre(selectedValue);
        }

        internal static int MonitorPorUser(string user)
        {
            return monitoresTable.PorUsuario(user).Value;
        }


        //Seguridad de Recovery

        internal static string ObtenerPregunta(int idRecovery)
        {
            return monitoresTable.PreguntaUsuario(idRecovery);
        }

        //Intenta el acceso de un usuario  Devuelve el ID o null
        internal static int? Acceso(string user, string pass, bool isMonitor)
        {
            int idUser;
            object aux;
            if (isMonitor)
            {
                aux = monitoresTable.Acceso(user, pass);
                if (aux == null)
                    return null;
                int.TryParse(aux.ToString(), out idUser);
            }
            else
            {
                aux = sociosTable.Acceso(user, pass);
                if (aux == null)
                    return null;
                int.TryParse(aux.ToString(), out idUser);
            }

            return idUser;
        }

        internal static void CambiarClave(bool isMonitor, int idRecoverUser)
        {
            var nuevaClave = GenerarClave();
            string nombre, mail, user;

            if (isMonitor)
            {
                var m = ObtenMonitor(idRecoverUser);
                m.Clave = nuevaClave;
                ActualizarAnyadir(m);

                nombre = m.Nombre;
                mail = m.Correo;
                user = m.Usuario;
            }
            else
            {
                var s = ObtenerSocio(idRecoverUser);
                s.Clave = nuevaClave;
                ActualizarAnyadir(s);
                nombre = s.Nombre;
                mail = s.Correo;
                user = s.Usuario;
            }

            EnviarNuevaClave(nombre, mail, user, nuevaClave);
        }

        internal static int? SocioPorUser(string usuario, string mail)
        {
            return sociosTable.ParUserMail(usuario, mail);
        }

        internal static int SocioPorUser(string usuario)
        {
            return sociosTable.PorUsuario(usuario).Value;
        }


        //Obtiene elementos por su ID

        internal static Actividad ObtenerActividad(int idActivity)
        {
            return new Actividad(actividadesTable.PorID(idActivity)[0]);
        }

        internal static Socio ObtenerSocio(int idUser)
        {
            var s = new Socio(sociosTable.PorID(idUser)[0]);

            return s;
        }

        internal static Socio ObtenerSocio(string idUser)
        {
            int.TryParse(idUser, out var ID);
            return ObtenerSocio(ID);
        }

        internal static Monitor ObtenMonitor(int idMon)
        {
            var m = new Monitor(monitoresTable.PorID(idMon)[0]);

            return m;
        }

        internal static Monitor ObtenMonitor(string idMon)
        {
            int.TryParse(idMon, out var ID);
            return ObtenMonitor(ID);
        }

        internal static Nivel ObtenNivel(string idText)
        {
            int.TryParse(idText, out var idNivel);
            return ObtenNivel(idNivel);
        }

        internal static Nivel ObtenNivel(int idNivel)
        {
            return new Nivel(nivelesTable.ObtenNivel(idNivel)[0]);
        }

        internal static TipoAcceso ObtenPrivilegios(int tipoAccesoID)
        {
            return new TipoAcceso(tipoAccesoTable.PorID(tipoAccesoID)[0]);
        }

        //Varios

        public static string GenerarClave()
        {
            return Membership.GeneratePassword(8,1).ToUpper();
        }

        internal static int? UserMailValido(string user, string mail, bool monitor)
        {
            if (monitor)
                return monitoresTable.ParUserMail(user, mail);
            return sociosTable.ParUserMail(user, mail);
        }

        internal static bool RespuestaCorrecta(int idRecovery, string respuesta)
        {
            if (monitoresTable.RespuestaCorrecta(idRecovery, respuesta) != null)
                return true;
            return false;
        }

        internal static int NivelAccesoPorIDMonitor(int idUser)
        {
            return ObtenPrivilegios(ObtenMonitor(idUser).TipoAccesoID).NivelAcceso;
        }

        //comprueba el formato de la fecha manual
        internal static bool FechaUsuarioCorrecta(int idRecovery, string fecha)
        {
            if (sociosTable.FechaCorrecta(idRecovery, fecha) != null)
                return true;
            return false;
        }

        //Devuelve un numero aleatorio
        internal static int Random(int min, int max)
        {
            var rnd = new Random();
            return rnd.Next(min, max);
            
        }

        internal static int Random(int max)
        {
            return Random(0, max);
        }

        //Devuelve una pregunta aleatoria para el registro
        internal static string PreguntaSeguridad()
        {
            var preguntas = new List<string>
            {
                "¿Nombre de tu primera mascota?", "¿Prefesión de tu abuelo?", "¿Drakis Drakaris?",
                "¿Mejor amigo de la infancia?"
            };

            return preguntas[Random(preguntas.Count - 1)];
        }

        //Comprueba que el usuario no está deshabilitado
        internal static bool Habilitado(int idUsuario, bool isMonitor)
        {
            var enabled = false;
            if (isMonitor)
            {
                if (monitoresTable.Habilitado(idUsuario) == 1)
                    enabled = true;
            }
            else
            {
                if (sociosTable.Habilitado(idUsuario) == 1)
                    enabled = true;
            }

            return enabled;
        }

        //Devuelve una lista de datos
        internal static List<string> MostrarActividades()
        {
            var acti = new List<string>();
            actividadesRows = actividadesTable.MostrarActividades();
            foreach (var r in actividadesRows)
            {
                var fila = "";
                fila += "<b><i>" + r.Nombre + "</b></i> con <b><i>" + r.MonitorID + "</b></i> socios (" + r.Monitor +
                        ")<br/>";
                acti.Add(fila);
            }

            return acti;
        }

        internal static List<string> TodosLosSociossActivos()
        {
            var socios = new List<string>();

            sociosRows = sociosTable.Activos(1);

            foreach (var row in sociosRows)
            {
                var aux = "(" + row.Usuario + ") " + row.Nombre;
                socios.Add(aux);
            }

            return socios;
        }

        internal static List<string> TodosLosMonitoresActivos()
        {
            var monitores = new List<string>();

            monitoresRows = monitoresTable.PorEstado(1);
            
            foreach (var row in monitoresRows)
            {
                var aux = "(" + row.Usuario + ") " + row.Nombre;
                monitores.Add(aux);
            }

            return monitores;
        }

        //Devuelve una lista de todos los tipos existentes de monitor
        public static List<string> TiposMonitor(int nivelAcceso)
        {
            tipoAccesoRows = tipoAccesoTable.GetData();

            return (from t in tipoAccesoRows where t.NivelAcceso < nivelAcceso select t.Nombre).ToList();
        }

        internal static List<string> TiposMonitor()
        {
            return TiposMonitor(10);
        }

        //Devuelve una lista de todos los niveles activos
        internal static List<string> TodosLosNivelesActivos()
        {
            var lista = new List<string>();
            nivelesRows = nivelesTable.NivelesActivos();

            foreach (var t in nivelesRows)
                lista.Add(t.Nombre);

            return lista;
        }

        //Devuelve una lista de todas las actividades
        internal static List<string> TodasLasActividades()
        {
            var lista = new List<string>();
            actividadesRows = actividadesTable.GetData();

            foreach (var t in actividadesRows)
                lista.Add(t.Nombre);

            return lista;
        }

        //Indica si existe un monitor proporcionando un mail
        internal static bool ExisteMailMonitor(string mail)
        {
            return null != monitoresTable.ExistePorMail(mail);
        }

        internal static bool ExisteMailMonitor(string mail, int idEditUser)
        {
            int? idLoc;
            if ((idLoc = monitoresTable.ExistePorMail(mail)) == null) return false;
            if (idLoc != idEditUser)
                return true;
            return false;
        }

        //Devuelve un monitor proporcionando un mail, si no null
        internal static int? MonitorByMail(string mail)
        {
            return monitoresTable.ExistePorMail(mail);
        }


        //Devuelve el ID del elemento en función de su nombre
        internal static int TipoAccesoPorNombre(string nombre)
        {
            return tipoAccesoTable.TipoPorNombre(nombre).Value;
        }

        internal static int NivelPorNombre(string nombre)
        {
            return nivelesTable.NivelPorNombre(nombre).Value;
        }


        // INSERT, UPDATE AND DELETE
        internal static void ActualizarAnyadir(Actividad act) // INSERT AND UPDATE
        {
            if (act.ActividadID < 1)
                actividadesTable.InsertQuery(act.MonitorID, act.Nombre);
            else
                actividadesTable.UpdateQuery(act.MonitorID, act.Nombre, act.ActividadID);
        }

        internal static void ActualizarAnyadir(Nivel lvl) // INSERT AND UPDATE
        {
            if (lvl.IdNivel < 1)
                nivelesTable.InsertQuery(lvl.Nombre, lvl.Estado);
            else
                nivelesTable.UpdateQuery(lvl.Nombre, lvl.Estado, lvl.IdNivel);
        }

        internal static void ActualizarAnyadir(Mensaje men) // INSERT AND UPDATE
        {
            mensajesRows = mensajesTable.GetData();
            var regMen = men.MensajeID > 0 ? mensajesRows.FindByID(men.MensajeID) : mensajesRows.NewMensajesRow();


            regMen.ID = men.MensajeID;
            regMen.Mensaje = men.Texto;
            regMen.RemID = men.MonID;
            regMen.DestID = men.SocID;
            regMen.Fecha = men.Fecha;


            if (men.MensajeID < 0)
                mensajesTable.Insert(regMen.Mensaje, regMen.RemID, regMen.DestID, regMen.Fecha);

            else
                mensajesTable.UpdateQuery(men.Texto, men.MonID, men.SocID, men.Fecha, men.MensajeID);

            mensajesRows = mensajesTable.GetData();
        }

        internal static void ActualizarAnyadirPuntos(Puntos ptos)

        {
            puntosRows = puntosTableAdapter.GetData();
            var regPtos = ptos.PuntosID > 0 ? puntosRows.FindByID(ptos.PuntosID) : puntosRows.NewPuntosRow();

            regPtos.SocID = ptos.SocID;
            regPtos.ID = ptos.PuntosID;
            regPtos.MonID = ptos.MonID;
            regPtos.Cantidad = ptos.Cantidad;
            regPtos.Fecha = ptos.Fecha;
            if (ptos.PuntosID < 0) puntosRows.AddPuntosRow(regPtos);

            puntosTableAdapter.Update(regPtos);
        }

        public static void ActualizarAnyadir(Monitor mon)
        {
            monitoresRows = monitoresTable.GetData(); //Necesario para actualizar

            ReptysDataSet.MonitoresRow regMon;
            regMon = mon.MonitorID > 0 ? monitoresRows.FindByID(mon.MonitorID) : monitoresRows.NewMonitoresRow();

            regMon.ID = mon.MonitorID;
            regMon.Nombre = mon.Nombre;
            regMon.Usuario = mon.Usuario;
            regMon.Clave = mon.Clave;
            regMon.Correo = mon.Correo;
            regMon.Estado = mon.Estado;
            regMon.TipoAccesoID = mon.TipoAccesoID;
            regMon.Pregunta = mon.Pregunta;
            regMon.Respuesta = mon.Respuesta;
            regMon.Telefono = mon.Telefono;


            if (mon.MonitorID < 0)
            {
                monitoresRows.AddMonitoresRow(regMon);
                monitoresTable.Update(regMon);
            }
            else
            {
                monitoresTable.UpdateQuery(regMon.TipoAccesoID, regMon.Nombre, regMon.Usuario, regMon.Clave,
                    regMon.Correo, regMon.Telefono, regMon.Pregunta, regMon.Respuesta, regMon.Estado, regMon.ID);
            }

            monitoresTable.GetData();
        }

        public static void ActualizarAnyadir(Socio soc)
        {
            sociosRows = sociosTable.GetData(); //Necesario para actualizar

            var regSoc = soc.SocioID > 0 ? sociosRows.FindByID(soc.SocioID) : sociosRows.NewSociosRow();

            regSoc.ID = soc.SocioID;
            regSoc.NivelID = soc.NivelID;
            regSoc.Nombre = soc.Nombre;
            regSoc.Usuario = soc.Usuario;
            regSoc.Clave = soc.Clave;
            regSoc.FechaNacimiento = soc.FechaNacimiento;
            regSoc.Correo = soc.Correo;
            regSoc.Estado = soc.Estado;
            regSoc.Actividad1ID = soc.ActID1;
            regSoc.Actividad2ID = soc.ActID2;


            if (soc.SocioID < 0)
            {
                sociosRows.AddSociosRow(regSoc);
                sociosTable.Update(regSoc);

                PuntosIniciales(PorUsuario(regSoc.Usuario, false));
            }
            else
            {
                sociosTable.UpdateQuery(soc.NivelID, soc.Nombre, soc.Correo, soc.Usuario, soc.Clave, soc.Estado,
                    soc.ActID1, soc.ActID2, soc.FechaNacimiento.ToString(), soc.SocioID);
            }

            sociosTable.GetData();
        }

        internal static void Anyadir(Mon_Niv mn)
        {
            mon_NivTable.InsertQuery(mn.MonID, mn.NivID);
        }


        private static void PuntosIniciales(int? idUser) //función interna
        {
            var ptos = new Puntos
            {
                MonID = SuperAdmin(), //Devuelve cualquier SA del sistema
                SocID = idUser.Value,
                Cantidad = 50
            };


            ActualizarAnyadirPuntos(ptos);
        }


        //Obtine el id de un administrador del sistema
        private static int SuperAdmin()
        {
            return monitoresTable.SuperAdmin()[0].ID;
        }

        //devuelve el ID actividad por Nombre
        internal static int? ActividadPorNombre(string selectedValue)
        {
            return actividadesTable.PorNombre(selectedValue);
        }


        //Comprueba si existe un usuario por ID
        internal static bool UserLibre(string user, bool isMonitor)
        {
            if (PorUsuario(user, isMonitor) == null)
                return true;
            return false;
        }

        //Comprueba si existe un usuario por ID o si pertenece al usuario indicado
        internal static bool UserLibrePropio(string user, bool isMonitor, int idUser)
        {
            var res = PorUsuario(user, isMonitor);
            if (res == null || res == idUser)
                return true;

            return false;
        }


        //Devuelve el ID de usuario por usuario
        internal static int? PorUsuario(string user, bool isMonitor)
        {
            if (!isMonitor) return sociosTable.PorUsuario(user);
            return monitoresTable.PorUsuario(user);
        }


        //MAIL
        private static void EnviarNuevaClave(string nombre, string mail, string user, string nuevaClave)
        {
            var fromAddress = new MailAddress("no-reply@drakis.dismasspain.com", "Drakis Reptys");
            var toAddress = new MailAddress(mail, nombre);
            const string fromPassword = "dismasDRAKIS@33";
            const string subject = "Recuperación de Contraseña";

            var mensaje = "Hola " + nombre + ", tal como nos pediste hemos recuperado la contraseña de " + user +
                          "\nTu nueva clave es: " + nuevaClave +
                          "\nTe recomiendo que la cambies en tu próximo login.\n\n<3 Drakis Reptys.";

            var body = mensaje;

            var smtp = new SmtpClient
            {
                Host = "smtp.ionos.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                try
                {
                    smtp.Send(message);
                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                }
                 
            }
        }

        internal static void EnviarMailBienvenida(string user, string mail, string nombre, string monitorOsocio)
        {
            var fromAddress = new MailAddress("no-reply@drakis.dismasspain.com", "Drakis Reptys");
            var toAddress = new MailAddress(mail, nombre);
            const string fromPassword = "dismasDRAKIS@33";
            const string subject = "Bienvenido";

            var mensaje = "Hola " + nombre +
                          ", soy Drakis Reptys y te doy la bienvenida a la aplicación.\nTu usuario para acceder como " +
                          monitorOsocio + " es " + user +
                          "\nTus datos ya están en el sistema a la espera de que un Administrador los valide.";

            var body = mensaje;

            var smtp = new SmtpClient
            {
                Host = "smtp.ionos.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                try
                {
                    smtp.Send(message);
                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                }
                 
            }
        }
    }
}