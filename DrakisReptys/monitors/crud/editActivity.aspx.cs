﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys.monitors.crud
{
    public partial class editActivity : Page
    {
        private Actividad act;
        private int idActivity;
        private bool isMonitor;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["idActivity"] == null || Session["existeSesion"] == null || Session["isMonitor"] == null ||
                Session["idUser"] == null)
            {
                Response.Redirect("/error/errorPage.aspx");
            }
            else
            {
                bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
                int.TryParse(Session["idActivity"].ToString(), out idActivity);
                if (!isMonitor)
                {
                    Response.Redirect("/error/errorPage.aspx");
                }
                else
                {
                    if (!IsPostBack)
                    {
                        ddlMonitor.Items.Add("SELECCIONE MONITOR");
                        var monitores = LNyAD.TodosLosMonitoresActivos();
                        foreach (var s in monitores) ddlMonitor.Items.Add(s);
                        ddlMonitor.SelectedIndex = 0;
                        if (idActivity > 0)
                        {
                            act = LNyAD.ObtenerActividad(idActivity);
                            tbNombre.Text = act.Nombre;
                            ddlMonitor.SelectedValue = LNyAD.NombreConUser(act.MonitorID);
                        }
                        else
                        {
                            act = new Actividad();
                        }
                    }
                }
            }
        }

        protected void cvNombre_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (LNyAD.ExisteActividad(idActivity, tbNombre.Text)) args.IsValid = false;
        }

        protected void cvMonitor_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ddlMonitor.SelectedIndex == 0) args.IsValid = false;
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("activitiesView.aspx");
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            Validate();
            if (!Page.IsValid) return;
            act = idActivity > 0 ? LNyAD.ObtenerActividad(idActivity) : new Actividad();

            act.Nombre = tbNombre.Text;
            act.MonitorID =
                LNyAD.MonitorPorUser(
                    ddlMonitor.SelectedValue.Substring(1, ddlMonitor.SelectedValue.IndexOf(')') - 1));
            LNyAD.ActualizarAnyadir(act);
            Response.Redirect("activitiesView.aspx");
        }
    }
}