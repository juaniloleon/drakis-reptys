﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys.control
{
    public partial class logup : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                switch (ddlMonitor.SelectedIndex)
                {
                    //monitor
                    case 1:
                        OcultarModificados();

                        FormularioMonitor();
                        btnGuardar.Visible = true;
                        break;
                    //socios
                    case 2:
                        OcultarModificados();

                        FormularioSocio();
                        btnGuardar.Visible = true;
                        break;
                    //No ha seleccionado, ocultar campos
                    default:
                        OcultarModificados();
                        break;
                }
            }
            else
            {
                CargarDDLMiembros();
                CargarDDLTipos();
                CargarDDLNiveles();
                CargarDDLActividad();

                ddlTipo.Visible = false;
                ddlNivel.Visible = false;
            }
        }

        private void OcultarModificados()
        {
            ddlTipo.Visible = false;
            ddlNivel.Visible = false;

            lbTipoNivel.Visible = false;
            ddlActividad1.Visible = false;
            ddlActividad2.Visible = false;
            lbTelefonoActividad.Visible = false;
            tbTelefono.Visible = false;
            lbPregunta.Visible = false;
            lbRespuesta.Visible = false;
            tbPregunta.Visible = false;
            tbRespuesta.Visible = false;
            btnGuardar.Visible = false;

            lbFecha.Visible = false;
            tbFecha.Visible = false;
        }


        //Adapta el formulario
        private void FormularioSocio()
        {
            ddlNivel.Visible = true;
            ddlActividad1.Visible = true;
            ddlActividad2.Visible = true;

            lbTelefonoActividad.Visible = true;
            lbTelefonoActividad.Text = "Actividades:";
            lbFecha.Visible = true;
            tbFecha.Visible = true;
        }

        //Carga las actividades existentes
        private void CargarDDLActividad()
        {
            ddlActividad1.Items.Add("Seleccione actividad 1");
            ddlActividad2.Items.Add("Seleccione actividad 2");

            var lista = LNyAD.TodasLasActividades();
            foreach (var s in lista)
            {
                ddlActividad1.Items.Add(s);
                ddlActividad2.Items.Add(s);
            }

            ddlActividad1.SelectedIndex = 0;
            ddlActividad2.SelectedIndex = 0;
        }


        //Adapta el formulario
        private void FormularioMonitor()
        {
            ddlTipo.Visible = true;
            tbTelefono.Visible = true;
            lbTelefonoActividad.Visible = true;
            lbTelefonoActividad.Text = "Teléfono:";
            lbPregunta.Visible = true;
            lbRespuesta.Visible = true;
            tbPregunta.Visible = true;
            tbRespuesta.Visible = true;
            tbPregunta.Text = LNyAD.PreguntaSeguridad();

            lbTipoNivel.Visible = true;
            lbTipoNivel.Text = "Tipo:";
        }

        //Carga los niveles existentes
        private void CargarDDLNiveles()
        {
            ddlNivel.Items.Add("Seleccione nivel");
            var lista = LNyAD.TodosLosNivelesActivos();
            foreach (var s in lista)
                ddlNivel.Items.Add(s);
            ddlNivel.SelectedIndex = 0;
            ddlNivel.Visible = true;
        }

        //Carga los tipos de monitor existente
        private void CargarDDLTipos()
        {
            ddlTipo.Items.Add("Seleccione tipo");
            var lista = LNyAD.TiposMonitor();
            foreach (var s in lista)
                ddlTipo.Items.Add(s);
            ddlTipo.SelectedIndex = 0;
            ddlTipo.Visible = true;
            //lbTipoNivel.Visible = true;
            //lbTipoNivel.Text = "Modo:";
        }

        //Elije si Monitor o Socio
        private void CargarDDLMiembros()
        {
            ddlMonitor.Items.Add("Seleccione uno");
            ddlMonitor.Items.Add("Monitor");
            ddlMonitor.Items.Add("Socio");
            ddlMonitor.SelectedIndex = 0;
        }


        protected void cvActividades_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ddlActividad2.SelectedIndex == ddlActividad1.SelectedIndex || ddlActividad1.SelectedIndex == 0 ||
                ddlActividad2.SelectedIndex == 0) args.IsValid = false;
        }

        protected void cvTlfno_ServerValidate(object source, ServerValidateEventArgs args)
        {
            int.TryParse(tbTelefono.Text, out var tlf);
            if (tlf > 999999999 || tlf < 600000000) args.IsValid = false;
        }

        protected void CustomTipo_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ddlTipo.SelectedIndex == 0) args.IsValid = false;
        }

        protected void CustomNivel_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ddlNivel.SelectedIndex == 0) args.IsValid = false;
        }

        protected void CustomMonitorMail_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (LNyAD.ExisteMailMonitor(tbMail.Text)) args.IsValid = false;
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("/control/login.aspx");
        }

        protected void btnGuardar_click(object sender, EventArgs e)
        {
            Validate("Generico"); //Campos comunes

            if (Page.IsValid)
            {
                if (ddlMonitor.SelectedIndex == 1) //Crear monitor
                {
                    Validate("vgMon"); //Campos exclusivos
                    if (Page.IsValid)
                    {
                        var mon = new Monitor
                        {
                            Clave = tbPass.Text,
                            Correo = tbMail.Text,
                            Estado = 0,
                            Nombre = tbNombre.Text,
                            Pregunta = tbPregunta.Text,
                            Respuesta = tbRespuesta.Text,
                            Telefono = tbTelefono.Text,
                            TipoAccesoID = LNyAD.TipoAccesoPorNombre(ddlTipo.SelectedValue),
                            Usuario = tbUser.Text
                        };

                        LNyAD.ActualizarAnyadir(mon);
                    }
                }
                else //Crear socio
                {
                    Validate("vgSoc"); //Campos exclusivos
                    if (Page.IsValid)
                    {
                        var soc = new Socio
                        {
                            Clave = tbPass.Text,
                            Correo = tbMail.Text,
                            Estado = 0,
                            Nombre = tbNombre.Text,
                            FechaNacimiento = DateTime.Parse(tbFecha.Text).Date,
                            NivelID = LNyAD.NivelPorNombre(ddlNivel.SelectedValue),
                            Usuario = tbUser.Text,
                            ActID1 = LNyAD.ActividadPorNombre(ddlActividad1.SelectedValue).Value,
                            ActID2 = LNyAD.ActividadPorNombre(ddlActividad2.SelectedValue).Value
                        };

                        LNyAD.ActualizarAnyadir(soc);
                    }
                }

                LNyAD.EnviarMailBienvenida(tbUser.Text, tbMail.Text, tbNombre.Text, ddlMonitor.SelectedValue);
            }

            if (Page.IsValid) Response.Redirect("login.aspx");
        }

        protected void customUser_ServerValidate(object source, ServerValidateEventArgs args)
        {
            switch (ddlMonitor.SelectedIndex)
            {
                case 1:
                {
                    if (!LNyAD.UserLibre(tbUser.Text, true))
                        args.IsValid = false;
                    break;
                }

                case 2:
                {
                    if (!LNyAD.UserLibre(tbUser.Text, false)) args.IsValid = false;
                    break;
                }
            }
        }

        protected void cvFecha_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = DateTime.TryParse(tbFecha.Text, out var fecha);

        }
    }
}