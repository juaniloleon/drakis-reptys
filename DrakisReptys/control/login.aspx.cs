﻿using System;
using System.Web.UI;
using DrakisReptys.Logic;

namespace DrakisReptys.control
{
    public partial class login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Solicitud"] == null) return;
            Session.Remove("Solicitud");
            lbError.Text = "La solicitud ha sido registrada correctamente";
            lbError.Visible = false;
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Session.RemoveAll();
            int? idUsuario;
            if ((idUsuario = LNyAD.Acceso(txbUser.Text, txbPass.Text, rbMonitor.Checked)) != null) //login correcto
            {
                lbError.Visible = false;

                Session.Add("idUser", idUsuario);
                Session.Add("existeSesion", true);


                if (!LNyAD.Habilitado(idUsuario.Value, rbMonitor.Checked))
                {
                    lbError.Text = "* SU USUARIO ESTÁ DESHABILITADO *";
                    lbError.Visible = true;
                    Session.RemoveAll();
                }
                else if (rbMonitor.Checked) //Monitor
                {
                    Session.Add("isMonitor", true);
                    Response.Redirect("/monitors/panel.aspx");
                }
                else
                {
                    //socio
                    Session.Add("isMonitor", false);
                    Response.Redirect("/members/panel.aspx");
                }
            }
            else
            {
                lbError.Text = "* Los datos introducidos no son correctos *";
                lbError.Visible = true;
            }
        }
    }
}