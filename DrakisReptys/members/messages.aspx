﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Styles/template.Master" AutoEventWireup="true" CodeBehind="messages.aspx.cs" Inherits="DrakisReptys.members.messages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row tm-content-row">
            <div class="col-12 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5" style="overflow: auto;">
                    <h2 class="tm-block-title">MIS MENSAJES</h2>
                    <div class="row">
                        <asp:Button ID="btnVolver" runat="server" CssClass="btn btn-danger" Text="Volver" OnClick="btnVolver_Click"/>
                    </div>
                    <br/>
                    <div class="row justify-content-center ">
                        <asp:GridView ID="gvMensajes" runat="server">

                            <EditRowStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"/>

                            <HeaderStyle BorderStyle="None" HorizontalAlign="Center"/>
                            <RowStyle BackColor="#374B5B" HorizontalAlign="Center" Width="100%" Wrap="False"/>
                            <AlternatingRowStyle BackColor="#5B7D99" BorderColor="#435C70" BorderWidth="0px"/>
                        </asp:GridView>
                    </div>
                    <asp:Label ID="lbZero" runat="server" Text="NO HAY MENSAJES QUE MOSTRAR"></asp:Label>
                    <br/>

                    <div class="row">
                        <asp:Button ID="btnVolver1" runat="server" CssClass="btn  btn-danger" Text="Volver" OnClick="btnVolver_Click"/>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>