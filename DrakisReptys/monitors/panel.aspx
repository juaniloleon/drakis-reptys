﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Styles/template.Master" AutoEventWireup="true" CodeBehind="panel.aspx.cs" Inherits="DrakisReptys.monitors.crud.panel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row tm-content-row">
            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5">
                    <h2 class="tm-block-title">PUNTVIAR</h2>
                    <div class="row">
                        <asp:DropDownList ID="ddlNivelesPuntear" runat="server" CssClass="btn btn-info" Width="70%" ValidationGroup="Puntear"></asp:DropDownList>
                        <asp:Button ID="btnPuntearAcceso" runat="server" CssClass="btn btn-success" Text="ACCEDER" Width="30%" OnClick="btnPuntearAcceso_Click" ValidationGroup="Puntear" ViewStateMode="Enabled"/>
                        <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="ddlNivelesPuntear" Display="Dynamic" ErrorMessage="* SELECCIONE UN NIVEL *" Font-Bold="True" ForeColor="Black" OnServerValidate="CustomValidator1_ServerValidate" ValidationGroup="Puntear"></asp:CustomValidator>
                    </div>
                    <hr/>
                    <h2 class="tm-block-title">MENSAJE RÁPIDO</h2>
                    <div class="row ">
                        <asp:DropDownList ID="ddlNivelMensaje" runat="server" CssClass="btn btn-info" ValidationGroup="qMess"></asp:DropDownList>

                        <asp:Button ID="btnVerMensajes" runat="server" CssClass="btn btn-outline-success" Text="VER MENSAJES" CausesValidation="False" OnClick="btnVerMensaje_Click"/>

                    </div><br/>
                    <div class="row justify-content-center">

                        <asp:TextBox ID="tbMensaje" runat="server" TextMode="MultiLine" Width="100%" MaxLength="100" Rows="2" ValidationGroup="qMess"></asp:TextBox>
                    </div><br/>
                    <div class="row justify-content-center">

                        <asp:Button ID="btnEnviarMensaje" runat="server" CssClass="btn btn-success" Text="ENVIAR" CausesValidation="False" OnClick="btnEnviarMensaje_Click" ValidationGroup="qMess"/>
                        <asp:CustomValidator ID="customLvlMess" runat="server" ControlToValidate="ddlNivelMensaje" Display="Dynamic" ErrorMessage="* SELECCIONE UN NIVEL *" Font-Bold="True" ForeColor="Black" OnServerValidate="customLvlMess_ServerValidate" ValidationGroup="qMess"></asp:CustomValidator>
                        <asp:CustomValidator ID="customMessL" runat="server" ControlToValidate="tbMensaje" Display="Dynamic" ErrorMessage="* EL MENSAJE DEBE TENER ENTRE 1 Y 75 CARÁCTERES *" Font-Bold="True" ForeColor="Black" OnServerValidate="customMessL_ServerValidate" ValidationGroup="qMess"></asp:CustomValidator>
                        <br/>

                    </div><asp:Label ID="lbMensajeEnviado" runat="server" Text="Se ha enviado el mensaje" Visible="False"></asp:Label>
                </div>
            </div>

            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5">
                    <h2 class="tm-block-title">
                        <asp:Label ID="lbSaludo" runat="server" Text="USER"></asp:Label>
                    </h2>
                    <div class="row">
                        <asp:Button ID="btnDatos" runat="server" CssClass="btn btn-block btn-dark" Text="MIS DATOS" OnClick="btnDatos_Click"/>
                    </div>
                    <hr/>
                    <asp:Label runat="server" Text="ADMINISTRAR" ID="lbAdministrar"></asp:Label>
                    <br/>
                    <div class="row justify-content-center">
                        <asp:Button ID="btnAdminMembers" runat="server" CssClass="btn btn-dark m-1" Text="SOCIOS" Width="45%" OnClick="btnAdminMembers_Click"/>
                        <!-- Ocultar en vista PC -->
                        <asp:Button ID="btnAdminMonitor" runat="server" CssClass="btn  btn-dark m-1" Text="MONITORES" Width="45%" OnClick="btnAdminMonitor_Click"/><br/>
                    </div>
                    <div class="row justify-content-center">

                        <asp:Button ID="btnAdminActivities" runat="server" CssClass="btn  btn-dark m-1" Text="ACTIVIDADES" Width="45%" OnClick="btnAdminActivities_Click"/>
                        <asp:Button ID="btnAdminLevels" runat="server" CssClass="btn btn- btn-dark m-1" Text="NIVELES" Width="45%" OnClick="btnAdminLevels_Click"/>

                    </div>
                    <div class="row justify-content-center">
                        <asp:Button ID="btnInspeccionMensajes" runat="server" CssClass="btn btn- btn-dark m-1" Text="MENSAJES" Width="45%" CausesValidation="False" OnClick="btnInspeccionMensajes_Click"/>
                        <asp:Button ID="btnPuntos" runat="server" CssClass="btn  btn-dark m-1" Text="PUNTOS" Width="45%" OnClick="btnPuntos_Click"/>

                    </div>


                </div>
            </div>
        </div>
    </div>
</asp:Content>