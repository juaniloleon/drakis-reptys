﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys.monitors.crud
{
    public partial class quickPoints : Page
    {
        private int idNivel;
        private int idUser;
        private bool isMonitor;
        private Monitor mon;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["existeSesion"] == null || Session["isMonitor"] == null || Session["idUser"] == null ||
                Session["idNivelQP"] == null)
            {
                Response.Redirect("/error/errorPage.aspx");
            }
            else
            {
                bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
                int.TryParse(Session["idUser"].ToString(), out idUser);
                if (!isMonitor)
                {
                    Response.Redirect("/error/errorPage.aspx");
                }
                else
                {
                    mon = LNyAD.ObtenMonitor(idUser);
                    int.TryParse(Session["idNivelQP"].ToString(), out idNivel);

                    if (idNivel > 0)
                    {
                        gvQuickPoints.DataSource = LNyAD.SociosPorNivelActivos(idNivel);
                    }
                    else
                    {
                        var acceso = LNyAD.NivelAccesoPorIDMonitor(idUser);
                        if (acceso < 10)
                            gvQuickPoints.DataSource = LNyAD.MisSocios(idUser);

                        else
                            gvQuickPoints.DataSource = LNyAD.Socios(1);
                    }


                    gvQuickPoints.DataBind();
                    if (gvQuickPoints.Rows.Count > 0)
                    {
                        lbGVvacio.Visible = false;

                        AdaptarDGVQP();
                    }
                    else
                    {
                        lbGVvacio.Visible = true;
                    }
                }
            }
        }

        private void AdaptarDGVQP()
        {
            string botones;

            gvQuickPoints.HeaderRow.Cells[11].Visible = false;
            gvQuickPoints.HeaderRow.Cells[2].Visible = false;
            gvQuickPoints.HeaderRow.Cells[3].Visible = false;
            gvQuickPoints.HeaderRow.Cells[5].Visible = false;
            gvQuickPoints.HeaderRow.Cells[6].Visible = false;
            gvQuickPoints.HeaderRow.Cells[7].Visible = false;
            gvQuickPoints.HeaderRow.Cells[8].Visible = false;
            gvQuickPoints.HeaderRow.Cells[9].Visible = false;
            gvQuickPoints.HeaderRow.Cells[10].Visible = false;

            foreach (GridViewRow r in gvQuickPoints.Rows)
            {
                botones = "<a  class=\"btn btn-danger\" href=\"qpAux.aspx?idMembers=" + r.Cells[2].Text +
                          "&P=-25\">-25</a>" +
                          "<a  class=\"btn  btn-success\" href=\"qpAux.aspx?idMembers=" + r.Cells[2].Text +
                          "&P=25\">+25</a>" +
                          "<br>" +
                          "<a  class=\"btn btn-danger\" href=\"qpAux.aspx?idMembers=" + r.Cells[2].Text +
                          "&P=-10\">-10</a>" +
                          "<a class=\"btn  btn-success\" href=\"qpAux.aspx?idMembers=" + r.Cells[2].Text +
                          "&P=10\">+10</a>";

                r.Cells[0].Text = LNyAD.TotalPuntos(r.Cells[2].Text);
                r.Cells[11].Visible = false;
                r.Cells[2].Visible = false;
                r.Cells[3].Visible = false;
                r.Cells[5].Visible = false;
                r.Cells[6].Visible = false;
                r.Cells[7].Visible = false;
                r.Cells[8].Visible = false;
                r.Cells[9].Visible = false;
                r.Cells[10].Visible = false;

                r.Cells[1].Text = botones;
            }
        }


        protected void btnVovler_Click2(object sender, EventArgs e)
        {
            Response.Redirect("panel.aspx");
        }

        protected void btnVolver2_Click(object sender, EventArgs e)
        {
            Response.Redirect("panel.aspx");
        }
    }
}