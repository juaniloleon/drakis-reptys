﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Styles/template.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DrakisReptys.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row tm-content-row">
            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5">
                    <h2 class="tm-block-title">RANKING</h2>

                    <div class="row">
                        <asp:DropDownList ID="ddlNivel" CssClass="btn-block btn-outline-light" runat="server" AutoPostBack="True"></asp:DropDownList>
                    </div>
                    <br/>
                    <asp:Panel ID="pnlToPrint" runat="server">

                        <div class="row align-content-center align-items-center justify-content-center" style="overflow-x: auto; overflow-y: auto;">
                            <asp:GridView ID="gvRanking" runat="server" ForeColor="White">
                                <Columns>
                                    <asp:BoundField HeaderText="POSICIÓN"/>
                                    <asp:BoundField HeaderText="SOCIO" ReadOnly="True"/>
                                    <asp:BoundField HeaderText="NIVEL"/>
                                </Columns>
                                <RowStyle BackColor="#374B5B" HorizontalAlign="Center" Width="100%" Wrap="False"/>
                                <AlternatingRowStyle BackColor="#5B7D99" BorderColor="#435C70" BorderWidth="0px"/>
                            </asp:GridView>
                            <asp:Label ID="lbVacio" runat="server" Text="No hay resultados para la consulta" CssClass="text-left" Visible="False" Font-Bold="True"></asp:Label>
                        </div>
                    </asp:Panel>

                    <br/>
                    <div class="row">
                        <button onclick="CallPrint()" class="btn btn-outline-warning mx-2">⎙</button>

                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5">
                    <h2 class="tm-block-title">Cumpleaños</h2>
                    <div class="row">
                        <asp:Label ID="lbCumpleList" runat="server" Text="Label"></asp:Label>
                    </div>
                </div>
                <hr/>

                <div class="tm-bg-primary-dark text-light p-5">
                    <h2 class="tm-block-title">Actividades</h2>
                    <div class="row">
                        <asp:Label ID="lbActividadsGV" runat="server" Text="Label"></asp:Label>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script>
        function CallPrint() {
            var printContent = document.getElementById('<%= pnlToPrint.ClientID %>');
            var printWindow = window.open("", "Print Panel", 'left=10000000000,top=10000000000,width=0,height=0');
            printWindow.document.write(printContent.innerHTML);
            printWindow.document.close();
            printWindow.focus();
            printWindow.print();
            printWindow.close();
        }
    </script>
</asp:Content>