﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Styles/template.Master" AutoEventWireup="true" CodeBehind="recover.aspx.cs" Inherits="DrakisReptys.control.recover" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row tm-content-row">
            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5">
                    <h2 class="tm-block-title">RECUPERAR CONTRASEÑA</h2>
                    <asp:Button ID="btnVolver" CssClass="btn btn-danger" runat="server" Text="VOLVER" OnClick="btnVolver_Click" CausesValidation="False"/>
                    <div class="row">
                        <asp:Label ID="lbUser" runat="server" CssClass="text-light" Text="Usuario"></asp:Label>
                        <asp:RequiredFieldValidator ID="reqUser" runat="server" ControlToValidate="tbUser" ErrorMessage="*CAMPO OBLIGATORIO*" Font-Bold="True" ForeColor="Black"></asp:RequiredFieldValidator>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="tbUser" CssClass="form-control " runat="server" MaxLength="8"></asp:TextBox>

                    </div>
                    <div class="row">
                        <asp:Label ID="lbMail" runat="server" CssClass="text-light" Text="Correo"></asp:Label>
                        <asp:RequiredFieldValidator ID="reqMail" runat="server" ControlToValidate="tbMail" ErrorMessage="*CAMPO OBLIGATORIO*" Font-Bold="True" ForeColor="Black"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="tbMail" ErrorMessage="*FORMATO NO VÁLIDO*" Font-Bold="True" ForeColor="Black" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="tbMail" CssClass="form-control " runat="server" MaxLength="100"></asp:TextBox>

                    </div>
                    <div class="row">
                        <asp:Label ID="lbTipo" runat="server" CssClass="text-light" Text="Tipo de usuario:"></asp:Label>
                        <asp:CustomValidator ID="CustomSelect" runat="server" ErrorMessage="*SELECCIONE UNA OPCIÓN*" ControlToValidate="ddlMonitor" Font-Bold="True" ForeColor="Black" ValidateEmptyText="True" OnServerValidate="CustomSelect_ServerValidate"></asp:CustomValidator>
                        <asp:CustomValidator ID="Custom" runat="server" ErrorMessage="*LA INFORMACIÓN NO ES VÁLIDA*" ControlToValidate="tbUser" Font-Bold="True" ForeColor="Black" OnServerValidate="Custom_ServerValidate" ValidateEmptyText="True"></asp:CustomValidator>
                    </div>
                    <div class="row">
                        <asp:DropDownList ID="ddlMonitor" CssClass="btn btn-outline-light" runat="server" AutoPostBack="True"></asp:DropDownList>
                    </div>
                    <br/>

                    <div class="row">
                        <asp:Button ID="btnVolver1" CssClass="btn btn-danger" runat="server" Text="VOLVER" OnClick="btnVolver_Click" Width="50%" CausesValidation="False"/>
                        <asp:Button ID="btnGuardar" CssClass="btn btn-success" runat="server" Text="SIGUIENTE" Width="50%" OnClick="btnGuardar_Click"/>


                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>