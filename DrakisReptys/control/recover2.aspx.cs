﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrakisReptys.Logic;

namespace DrakisReptys.control
{
    public partial class recover2 : Page
    {
        private int idRecoverUser;
        private bool isMonitor;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["idRecovery"] != null)
            {
                int.TryParse(Session["idRecovery"].ToString(), out idRecoverUser);
                bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
                if (IsPostBack)
                {
                }
                else
                {
                    if (isMonitor)
                    {
                        lbPregunta.Text = LNyAD.ObtenerPregunta(idRecoverUser);
                        tbRespuesta.Attributes["type"] = "SingleLine";
                    }
                    else
                    {
                        lbPregunta.Text = "Cumpleaños: ";
                        tbRespuesta.Attributes["type"] = "Date";
                    }
                }
            }
            else
            {
                Session.RemoveAll();
                Response.Redirect("/Default.aspx");
            }
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Session.RemoveAll();
            Response.Redirect("/Default.aspx");
        }

        protected void Custom_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (isMonitor && !LNyAD.RespuestaCorrecta(idRecoverUser, tbRespuesta.Text))
                args.IsValid = false;
            else if (!isMonitor && !LNyAD.FechaUsuarioCorrecta(idRecoverUser, tbRespuesta.Text)) args.IsValid = false;
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                LNyAD.CambiarClave(isMonitor, idRecoverUser);
                Response.Redirect("/control/login.aspx");
            }
        }
    }
}