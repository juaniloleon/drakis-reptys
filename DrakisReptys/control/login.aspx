﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Styles/template.Master" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="DrakisReptys.control.login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row tm-content-row">
            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5">
                    <h2 class="tm-block-title">ACCESO</h2>
                    <p class="tm-block-title">
                        <asp:Label ID="lbError" runat="server" Font-Bold="True" ForeColor="Black" Text="* LOS DATOS INTRODUCIDOS NO SON VÁLIDOS *" Visible="False"></asp:Label>
                    </p>
                    <div class="simple-login-container">
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <asp:Label ID="lbUser" runat="server" Text="Usuario" CssClass="text-center text-light"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txbUser" CssClass="text-danger" ErrorMessage="Debe introducir un usuario">*</asp:RequiredFieldValidator>
                                <asp:TextBox ID="txbUser" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <asp:Label ID="lbPass" runat="server" Text="Contraseña" CssClass="text-center text-light"></asp:Label>

                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txbPass" CssClass="text-danger" ErrorMessage="Debe introducir un usuario">*</asp:RequiredFieldValidator>

                                <asp:TextBox ID="txbPass" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <asp:Button ID="btnLogin" runat="server" Text="Acceder" CssClass="btn btn-block btn-success btn-login" OnClick="btnLogin_Click" AccessKey=" "/>
                            </div>
                        </div>
                        <div class="row align-content-center text-light text-center">
                            <div class="radio pr-3">

                                <asp:Label ID="lbMonitor" runat="server" Text="">
                                    <asp:RadioButton ID="rbMonitor" runat="server" GroupName="Tipo" Checked="True"/>
                                    Monitor
                                </asp:Label>

                            </div>
                            <div class="radio pl-3">

                                <asp:Label ID="lbl" runat="server" Text="">
                                    <asp:RadioButton ID="rbSocio" runat="server" GroupName="Tipo"/>
                                    Socio
                                </asp:Label>

                            </div>
                        </div>

                    </div>
                    <div class="row text-light ">
                        <a href="recover.aspx" class="btn btn-outline-info">Recuperar contraseña</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                        <a href="logup.aspx" class="btn btn-outline-info">Registrar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>