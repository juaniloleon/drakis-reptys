﻿using System;
using System.Web.UI;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys.monitors.crud
{
    public partial class qpAux : Page
    {
        private int idNivel;
        private int idUser;
        private bool isMonitor;
        private Monitor mon;
        private readonly Puntos p = new Puntos();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["existeSesion"] == null || Session["isMonitor"] == null || Session["idUser"] == null ||
                Session["idNivelQP"] == null)
            {
                Response.Redirect("/error/errorPage.aspx");
            }
            else
            {
                bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
                int.TryParse(Session["idUser"].ToString(), out idUser);
                if (!isMonitor)
                {
                    Response.Redirect("/error/errorPage.aspx");
                }
                else
                {
                    mon = LNyAD.ObtenMonitor(idUser);
                    int.TryParse(Session["idNivelQP"].ToString(), out idNivel);

                    string aux;
                    int idSoc, points;

                    aux = Request.QueryString["idMembers"];
                    int.TryParse(aux, out idSoc);
                    aux = Request.QueryString["P"];
                    int.TryParse(aux, out points);

                    p.MonID = idUser;
                    p.Fecha = DateTime.Now;
                    p.SocID = idSoc;
                    p.Cantidad = points;


                    if (idSoc != 0 && idUser != 0) LNyAD.ActualizarAnyadirPuntos(p);


                    Response.Redirect("quickPoints.aspx");
                }
            }
        }

        protected void btnVOLVER_Click(object sender, EventArgs e)
        {
        }
    }
}