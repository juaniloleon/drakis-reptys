﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys.monitors.crud
{
    public partial class allMessages : Page
    {
        private int idUser;
        private bool isMonitor;
        private Monitor mon;
        private TipoAcceso priv;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["existeSesion"] == null || Session["isMonitor"] == null || Session["idUser"] == null)
            {
                Response.Redirect("/error/errorPage.aspx");
            }
            else
            {
                bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
                int.TryParse(Session["idUser"].ToString(), out idUser);
                mon = LNyAD.ObtenMonitor(idUser);
                priv = LNyAD.ObtenPrivilegios(mon.TipoAccesoID);
                if (!isMonitor || priv.NivelAcceso < 10)
                {
                    Response.Redirect("/error/errorPage.aspx");
                }
                else
                {
                    if (IsPostBack)
                    {
                        CargarDatos();
                    }
                    else
                    {
                        lbAvisoVacio.Visible = false;

                        CargarDDL();
                        gvDatos.DataSource = LNyAD.MensajesAdmin(0);
                        gvDatos.DataBind();

                        if (gvDatos.Rows.Count > 0)
                            AdaptarGV();
                        else
                            lbAvisoVacio.Visible = true;
                    }
                }
            }
        }

        private void CargarDatos()
        {
            int idMonitor;

            if (ddlMonitor.SelectedIndex == 0)
            {
                idMonitor = 0;
            }
            else
            {
                var user = ddlMonitor.SelectedValue.Substring(1, ddlMonitor.SelectedValue.IndexOf(')') - 1);
                idMonitor = LNyAD.MonitorPorUser(user);
            }


            gvDatos.DataSource = LNyAD.MensajesAdmin(idMonitor);
            gvDatos.DataBind();
            lbAvisoVacio.Visible = false;

            if (gvDatos.Rows.Count > 0)
                AdaptarGV();

            else
                lbAvisoVacio.Visible = true;
        }

        private void AdaptarGV()
        {
            gvDatos.HeaderRow.Cells[1].Visible = false;
            gvDatos.HeaderRow.Cells[3].Text = "Monitor";

            gvDatos.HeaderRow.Cells[4].Text = "Socio";
            if (ddlMonitor.SelectedIndex != 0)
            {
                gvDatos.HeaderRow.Cells[6].Visible = false;
                gvDatos.HeaderRow.Cells[3].Visible = false;
            }

            foreach (GridViewRow r in gvDatos.Rows)
            {
                r.Cells[1].Visible = false;
                r.Cells[5].Text = LNyAD.FormatoEuropeoFecha(r.Cells[5].Text);
                if (ddlMonitor.SelectedIndex != 0)
                {
                    r.Cells[4].Text = r.Cells[6].Text;
                    r.Cells[3].Visible = false;
                    r.Cells[6].Visible = false;
                }
                else
                {
                    r.Cells[4].Text = LNyAD.ObtenerSocio(r.Cells[4].Text).Nombre;
                    r.Cells[3].Text = LNyAD.ObtenMonitor(r.Cells[3].Text).Nombre;
                }
            }
        }

        private void CargarDDL()
        {
            ddlMonitor.Items.Add("TODOS LOS MONITORES");
            var monitores = LNyAD.TodosLosMonitoresActivos();

            foreach (var s in monitores)
                ddlMonitor.Items.Add(s);
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("/monitors/panel.aspx");
        }

        protected void gvMensajes_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            LNyAD.BorrarMensaje(gvDatos.Rows[e.RowIndex].Cells[1].Text);
            Response.Redirect("allMessages.aspx");
        }
    }
}