﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Styles/template.Master" AutoEventWireup="true" CodeBehind="levelsView.aspx.cs" Inherits="DrakisReptys.monitors.crud.levelsView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row tm-content-row">
            <div class="col-12 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5 ">
                    <h2 class="tm-block-title">ADMINISTRAR NIVELES</h2>
                    <br/>
                    <div class="row">
                        <asp:Button ID="btnVolver" CssClass="btn btn-danger" runat="server" Text="VOLVER" OnClick="btnVolver_Click"/>
                        <asp:Button ID="btnRelation" CssClass="btn btn-danger mx-2" runat="server" Text="ASIGNAR MONITORES" OnClick="btnRelations_Click"/>


                        <asp:Button ID="btnNuevo" CssClass="btn btn-warning mx-2" runat="server" Text="NUEVO" OnClick="btnNew_Click"/>
                        <asp:DropDownList ID="ddlBusqueda" CssClass="my-2 btn btn-dark my-2" runat="server" AutoPostBack="True" Width="25%"></asp:DropDownList>

                        <asp:Button ID="btnPrint" CssClass="btn btn-warning mx-2" runat="server" Text="⎙" OnClientClick="javascript:window.print();"/>

                    </div>

                    <br/>
                    <asp:Label ID="lbAviso" runat="server" BackColor="White" Font-Bold="True" Font-Italic="True" Font-Size="Medium" ForeColor="Black"></asp:Label>
                    <br/>
                    <div class="row justify-content-center" style="overflow: auto">
                        <asp:Label ID="lbAvisoVacio" runat="server" Text="NO HAY REGISTROS QUE MOSTRAR"></asp:Label>
                        <asp:GridView ID="gvDatos" runat="server" OnRowDeleting="gvDatos_RowDeleting1" OnRowEditing="gvDatos_RowEditing1">
                            <Columns>
                                <asp:CommandField ButtonType="Button" CausesValidation="False" DeleteText="𝒳" EditText="✎" InsertVisible="False" ShowCancelButton="False" ShowDeleteButton="True" ShowEditButton="True"/>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <br/>

                    <div class="row">
                        <asp:Button ID="btnVolver1" CssClass="btn btn-danger" runat="server" Text="VOLVER" OnClick="btnVolver_Click"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>