﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Styles/template.Master" AutoEventWireup="true" CodeBehind="errorPage.aspx.cs" Inherits="DrakisReptys.error.errorPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row tm-content-row">
            <div class="col-sm-12 col-md-12 col-lg-8 col-xl-8 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5">
                    <h2 class="tm-block-title">ERROR</h2>

                    <div class="row">
                        <p>No sabemos el motivo, pero se ha producido un error. Lo más probable es que los monos se hayan sublevado y estén arrojando platanos al servidor central. Mientras mandamos a alguien con Petente (el encargado de enchufar los cables) te sugiero que pulses el botón de abajo y lo intentes de nuevo.</p>
                    </div>

                    <div class="row">
                        <p> Te dejo algo de música mientras lo solucionamos</p>
                    </div>


                    <div class="row justify-content-center">
                        <asp:Button ID="btnVolver" CssClass="btn btn-block btn-dark" runat="server" Text="VOLVER" OnClick="btnVolver_Click"/>

                        <iframe id="ytplayer" width="100%" height="300" src="https://www.youtube.com/embed/dQw4w9WgXcQ?;autoplay=1&controls=0&fs=0&color=red"></iframe>

                    </div>


                </div>
            </div>
        </div>
    </div>
</asp:Content>