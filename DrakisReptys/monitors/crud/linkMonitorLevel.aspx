﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Styles/template.Master" AutoEventWireup="true" CodeBehind="linkMonitorLevel.aspx.cs" Inherits="DrakisReptys.monitors.crud.linksMonitorLevel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row tm-content-row">
            <div class="col-sm-12 col-md-12 col-lg-10 col-xl-10 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5">
                    <h2 class="tm-block-title">AGINAR MONITORES Y NIVELES</h2>
                    <div class="row">
                        <asp:Button ID="btnVolver" CssClass="btn btn-danger" runat="server" Text="VOLVER" OnClick="btnVolver_Click"/>
                    </div>
                    <br/>

                    <div class="row">
                        <asp:DropDownList ID="ddlOrden" CssClass="btn btn-dark mr-1" runat="server" AutoPostBack="True" Width="25%"></asp:DropDownList>
                    </div>
                    <hr/>
                    <div class="row">

                        <asp:Button ID="btnNew" CssClass="btn btn-success ml-1" runat="server" Text="AÑADIR" OnClick="btnNew_Click" ValidationGroup="addLink"/>

                        <asp:DropDownList ID="ddlLeves" CssClass="btn btn-outline-warning" runat="server"></asp:DropDownList>

                        <asp:DropDownList ID="ddlMonitors" CssClass="btn btn-outline-" runat="server"></asp:DropDownList>
                        <asp:CustomValidator ID="cvMon" runat="server" ControlToValidate="ddlMonitors" ErrorMessage="*SELECCIONE MONITOR*" Font-Bold="True" ForeColor="Black" ValidationGroup="addLink" OnServerValidate="cvMon_ServerValidate"></asp:CustomValidator>
                        <asp:CustomValidator ID="cvLev" runat="server" ControlToValidate="ddlLeves" ErrorMessage="*SELECCIONE NIVEL*" Font-Bold="True" ForeColor="Black" ValidationGroup="addLink" OnServerValidate="cvLev_ServerValidate"></asp:CustomValidator>
                        <asp:CustomValidator ID="cvLink" runat="server" ControlToValidate="ddlMonitors" ErrorMessage="*YA EXISTE ESA RELACIÓN*" Font-Bold="True" ForeColor="Black" ValidationGroup="addLink" OnServerValidate="cvLink_ServerValidate"></asp:CustomValidator>
                    </div>
                    <br/>
                    <asp:Label ID="lbHecho" runat="server" Text="Eliminado" BackColor="White" Font-Italic="True" ForeColor="Black" Visible="False"></asp:Label>
                    <br/>

                    <div class="row justify-content-center" style="overflow: auto">
                        <asp:GridView ID="gvDatos" runat="server" OnRowDeleting="gvDatos_RowDeleting">
                            <Columns>
                                <asp:CommandField CausesValidation="False" DeleteText="𝓧" ControlStyle-CssClass="btn btn-dark" InsertVisible="False" ShowCancelButton="False" ShowDeleteButton="True"/>
                            </Columns>
                        </asp:GridView>
                        <asp:Label ID="lbAvisoVacio" runat="server" Text="NO HAY DATOS QUE MOSTRAR"></asp:Label>

                    </div>

                    <br/>
                    <div class="row">
                        <asp:Button ID="btnVolver1" CssClass="btn btn-danger" runat="server" Text="VOLVER" OnClick="btnVolver_Click"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>