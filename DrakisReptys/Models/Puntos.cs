﻿using System;

namespace DrakisReptys.Models
{
    public class Puntos
    {
        public Puntos()
        {
            PuntosID = -1;
            Fecha = DateTime.Now;
        }

        public Puntos(int puntosID, int monID, int socID, int cantidad, DateTime fecha)
        {
            PuntosID = puntosID;
            MonID = monID;
            SocID = socID;
            Cantidad = cantidad;
            Fecha = fecha;
        }

        public int PuntosID { get; set; }

        public int MonID { get; set; }

        public int SocID { get; set; }

        public int Cantidad { get; set; }

        public DateTime Fecha { get; set; }
    }
}