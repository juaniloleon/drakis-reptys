﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys.monitors.crud
{
    public partial class linksMonitorLevel : Page
    {
        private int idUser;
        private bool isMonitor;
        private Monitor mon;
        private TipoAcceso priv;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["existeSesion"] == null || Session["isMonitor"] == null || Session["idUser"] == null)
            {
                Response.Redirect("/error/errorPage.aspx");
            }
            else
            {
                bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
                int.TryParse(Session["idUser"].ToString(), out idUser);
                mon = LNyAD.ObtenMonitor(idUser);
                priv = LNyAD.ObtenPrivilegios(mon.TipoAccesoID);
                if (!isMonitor || priv.NivelAcceso < 10)
                {
                    Response.Redirect("/error/errorPage.aspx");
                }
                else
                {
                    mon = LNyAD.ObtenMonitor(idUser);
                    priv = LNyAD.ObtenPrivilegios(mon.TipoAccesoID);
                    if (IsPostBack)
                    {
                        CargarDatos();
                    }
                    else
                    {
                        lbAvisoVacio.Visible = false;

                        CargarDDL();
                        gvDatos.DataSource = LNyAD.RelMonNivel();
                        gvDatos.DataBind();


                        if (gvDatos.Rows.Count > 0)
                            AdaptarGV();
                        else
                            lbAvisoVacio.Visible = true;
                    }
                }
            }
        }

        private void CargarDatos()
        {
            if (ddlOrden.SelectedIndex == 0) gvDatos.DataSource = LNyAD.RelMonNivel();
            else gvDatos.DataSource = LNyAD.RelMonitorNiv();

            gvDatos.DataBind();
            AdaptarGV();
        }

        private void AdaptarGV()
        {
            if (gvDatos.Rows.Count > 0)
            {
                lbAvisoVacio.Visible = false;

                gvDatos.HeaderRow.Cells[1].Visible = false;
                gvDatos.HeaderRow.Cells[2].Text = "Monitor";
                gvDatos.HeaderRow.Cells[3].Text = "Nivel";


                foreach (GridViewRow r in gvDatos.Rows)
                {
                    r.Cells[1].Visible = false;

                    r.Cells[2].Text = LNyAD.ObtenMonitor(Convert.ToInt32(r.Cells[2].Text)).Nombre;
                    r.Cells[3].Text = LNyAD.NombreNivel(Convert.ToInt32(r.Cells[3].Text));
                }
            }
            else
            {
                lbAvisoVacio.Visible = true;
            }
        }

        private void CargarDDL()
        {
            ddlOrden.Items.Add("POR NIVEL");
            ddlOrden.Items.Add("POR MONITOR");

            var monitores = LNyAD.TodosLosMonitoresActivos();
            var niveles = LNyAD.TodosLosNivelesActivos();

            ddlLeves.Items.Add("NIVEL");
            ddlMonitors.Items.Add("MONITOR");

            foreach (var s in monitores) ddlMonitors.Items.Add(s);
            foreach (var s in niveles) ddlLeves.Items.Add(s);
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("/monitors/CRUD/levelsView.aspx");
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Validate("addLink");
            if (Page.IsValid)
            {
                var mn = new Mon_Niv();

                var user = ddlMonitors.SelectedValue.Substring(1, ddlMonitors.SelectedValue.IndexOf(')') - 1);

                mn.NivID = LNyAD.NivelPorNombre(ddlLeves.SelectedValue);
                mn.MonID = LNyAD.MonitorPorUser(user);

                LNyAD.Anyadir(mn);
                lbHecho.Visible = true;
                lbHecho.Text = "Añadido";
                Response.Redirect("linkMonitorLevel.aspx");
            }
        }

        protected void cvLink_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ddlLeves.SelectedIndex == 0 || ddlMonitors.SelectedIndex == 0)
                return;

            var user = ddlMonitors.SelectedValue.Substring(1, ddlMonitors.SelectedValue.IndexOf(')') - 1);

            if (LNyAD.ExisteLink(LNyAD.MonitorPorUser(user), LNyAD.NivelPorNombre(ddlLeves.SelectedValue)))
                args.IsValid = false;
        }

        protected void cvMon_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ddlMonitors.SelectedIndex == 0)
                args.IsValid = false;
        }

        protected void cvLev_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ddlLeves.SelectedIndex == 0)
                args.IsValid = false;
        }

        protected void gvDatos_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            LNyAD.BorrarLink(gvDatos.Rows[e.RowIndex].Cells[1].Text);
            lbHecho.Visible = true;
            lbHecho.Text = "Borrado";
            Response.Redirect("linkMonitorLevel.aspx");
        }
    }
}