﻿using System;
using DrakisReptys.Logic;

namespace DrakisReptys.Models
{
    public class Socio
    {
        public Socio()
        {
            SocioID = -1;
        }

        public Socio(ReptysDataSet.SociosRow sociosRow)
        {
            SocioID = sociosRow.ID;
            NivelID = sociosRow.NivelID;
            Estado = sociosRow.Estado;
            ActID1 = sociosRow.Actividad1ID;
            ActID2 = sociosRow.Actividad2ID;
            Nombre = sociosRow.Nombre;
            Usuario = sociosRow.Usuario;
            Clave = sociosRow.Clave;
            Correo = sociosRow.Correo;
            FechaNacimiento = sociosRow.FechaNacimiento;
        }

        public Socio(int socioID, int nivelID, int estado, int actID1, int actID2, string nombre, string usuario,
            string clave, string correo, DateTime fechaNacimiento)
        {
            SocioID = socioID;
            NivelID = nivelID;
            Estado = estado;
            ActID1 = actID1;
            ActID2 = actID2;
            Nombre = nombre;
            Usuario = usuario;
            Clave = clave;
            Correo = correo;
            FechaNacimiento = fechaNacimiento;
        }

        public int SocioID { get; set; }

        public int NivelID { get; set; }

        public int Estado { get; set; }

        public int ActID1 { get; set; }

        public int ActID2 { get; set; }

        public string Nombre { get; set; }

        public string Usuario { get; set; }

        public string Clave { get; set; }

        public string Correo { get; set; }

        public DateTime FechaNacimiento { get; set; }
    }
}