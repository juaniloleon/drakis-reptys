﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Styles/template.Master" AutoEventWireup="true" CodeBehind="pointsView.aspx.cs" Inherits="DrakisReptys.monitors.crud.pointsView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row tm-content-row">
            <div class="col-sm-12 col-md-12 col-lg-10 col-xl-10 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5">
                    <h2 class="tm-block-title">REGISTRO DE PUNTOS</h2>
                    <div class="row">
                        <asp:Button ID="btnVolver" CssClass="btn btn-danger" runat="server" Text="VOLVER" OnClick="btnVolver_Click"/>
                    </div>
                    <hr/>
                    <div class="row">

                        <asp:Button ID="btnNew" CssClass="btn btn-success ml-1" runat="server" Text="AÑADIR" OnClick="btnNew_Click" ValidationGroup="addLink"/>

                        <asp:DropDownList ID="ddlMonitors" CssClass="btn btn-outline-" runat="server"></asp:DropDownList>

                        <asp:DropDownList ID="ddlSoc" CssClass="btn btn-outline-warning" runat="server"></asp:DropDownList>
                        <asp:TextBox TextMode="Number" CssClass="form-control" runat="server" min="-50" max="200" step="5" ID="tbPuntos" Width="15%" Text="0"></asp:TextBox>
                    </div>
                    <div class="row">
                        <asp:CustomValidator ID="cvMon" runat="server" ControlToValidate="ddlMonitors" ErrorMessage="*SELECCIONE MONITOR*" Font-Bold="True" ForeColor="Black" ValidationGroup="addLink" OnServerValidate="cvMon_ServerValidate"></asp:CustomValidator>
                        <asp:CustomValidator ID="cvSoc" runat="server" ControlToValidate="ddlSoc" ErrorMessage="*SELECCIONE SOCIO*" Font-Bold="True" ForeColor="Black" ValidationGroup="addLink" OnServerValidate="cvSoc_ServerValidate"></asp:CustomValidator>
                    </div>
                    <hr/>
                    <asp:Label ID="lbHecho" runat="server" Text="Eliminado" BackColor="White" Font-Italic="True" ForeColor="Black" Visible="False"></asp:Label>
                    <br/>

                    <div class="row justify-content-center" style="overflow: auto">
                        <asp:GridView ID="gvDatos" runat="server" OnRowDeleting="gvDatos_RowDeleting">
                            <AlternatingRowStyle BackColor="#2B3946"/>
                            <Columns>
                                <asp:CommandField CausesValidation="False" DeleteText="𝓧" ControlStyle-CssClass="btn btn-dark" InsertVisible="False" ShowCancelButton="False" ShowDeleteButton="True">
                                    <ControlStyle CssClass="btn btn-dark"></ControlStyle>
                                </asp:CommandField>
                            </Columns>
                            <HeaderStyle BackColor="#2B3946"/>
                            <RowStyle HorizontalAlign="Center" VerticalAlign="Middle"/>
                        </asp:GridView>
                        <asp:Label ID="lbAvisoVacio" runat="server" Text="NO HAY DATOS QUE MOSTRAR"></asp:Label>

                    </div>

                    <br/>
                    <div class="row">
                        <asp:Button ID="btnVolver1" CssClass="btn btn-danger" runat="server" Text="VOLVER" OnClick="btnVolver_Click"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>