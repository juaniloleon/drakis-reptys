﻿using DrakisReptys.Logic;

namespace DrakisReptys.Models
{
    public class Nivel
    {
        public Nivel()
        {
            IdNivel = -1;
        }

        public Nivel(ReptysDataSet.NivelesRow nivelesRow)
        {
            IdNivel = nivelesRow.ID;
            Estado = nivelesRow.Estado;
            Nombre = nivelesRow.Nombre;
        }

        public Nivel(int idNivel, int estado, string nombre)
        {
            IdNivel = idNivel;
            Estado = estado;
            Nombre = nombre;
        }

        public int IdNivel { get; set; }

        public int Estado { get; set; }

        public string Nombre { get; set; }
    }
}