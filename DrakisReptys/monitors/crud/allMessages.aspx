﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Styles/template.Master" AutoEventWireup="true" CodeBehind="allMessages.aspx.cs" Inherits="DrakisReptys.monitors.crud.allMessages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row tm-content-row">
            <div class="col-12 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5" style="overflow: auto;">
                    <h2 class="tm-block-title">SUPERVISAR MENSAJES</h2>
                    <div class="row">
                        <asp:Button ID="btnVolver" runat="server" CssClass="btn btn-danger" Text="VOLVER" OnClick="btnVolver_Click"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddlMonitor" runat="server" CssClass="btn btn-info" ValidationGroup="" AutoPostBack="True"></asp:DropDownList>

                        &nbsp;
                        <asp:Button ID="btnPrint" runat="server" CssClass="btn btn-outline-warning mx-2" OnClientClick="javascript:window.print();" Text="⎙"/>
                    </div>
                    <br/>
                    <div class="row justify-content-center ">
                        <asp:GridView ID="gvDatos" runat="server" OnRowDeleting="gvMensajes_RowDeleting">

                            <Columns>
                                <asp:CommandField ButtonType="Button" DeleteText="✘" ControlStyle-CssClass="btn btn-dark" ShowDeleteButton="True"/>
                            </Columns>

                            <EditRowStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"/>

                            <HeaderStyle BorderStyle="None" HorizontalAlign="Center"/>
                            <RowStyle BackColor="#374B5B" HorizontalAlign="Center" Width="100%" Wrap="False"/>
                            <AlternatingRowStyle BackColor="#5B7D99" BorderColor="#435C70" BorderWidth="0px"/>
                        </asp:GridView>
                        <asp:Label ID="lbAvisoVacio" runat="server" Text="NO HAY MENSAJES QUE MOSTRAR"></asp:Label>
                    </div>
                    <br/>

                    <div class="row">
                        <asp:Button ID="btnVolver1" runat="server" CssClass="btn  btn-danger" Text="VOLVER" OnClick="btnVolver_Click"/>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>