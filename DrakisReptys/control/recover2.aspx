﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Styles/template.Master" AutoEventWireup="true" CodeBehind="recover2.aspx.cs" Inherits="DrakisReptys.control.recover2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row tm-content-row">
            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5">
                    <h2 class="tm-block-title">RECUPERAR CONTRASEÑA</h2>
                    <asp:Button ID="btnVolver" CssClass="btn btn-danger" runat="server" Text="VOLVER" OnClick="btnVolver_Click" CausesValidation="False"/>
                    <div class="row">
                        <asp:Label ID="lbPregunta" runat="server" Text="TEXT"></asp:Label>
                        <asp:CustomValidator ID="Custom" runat="server" ControlToValidate="tbRespuesta" ErrorMessage="*LA RESPUESTA NO ES VÁLIDA*" Font-Bold="True" ForeColor="Black" OnServerValidate="Custom_ServerValidate"></asp:CustomValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tbRespuesta" ErrorMessage="*EL CAMPO ES OBLIGATORIO*" Font-Bold="True" Font-Italic="False" ForeColor="Black"></asp:RequiredFieldValidator>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="tbRespuesta" CssClass="form-control" runat="server" MaxLength="50"></asp:TextBox>
                    </div>
                    <div class="row">
                        <asp:Button ID="btnVolver1" CssClass="btn btn-danger" runat="server" Text="VOLVER" OnClick="btnVolver_Click" Width="50%" CausesValidation="False"/>
                        <asp:Button ID="btnEnviar" CssClass="btn btn-success" runat="server" Text="ENVIAR CLAVE POR CORREO" Width="50%" CausesValidation="true" OnClick="btnEnviar_Click"/>

                    </div>
                </div>

            </div>
        </div>
    </div>
</asp:Content>