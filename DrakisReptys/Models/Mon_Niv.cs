﻿namespace DrakisReptys.Models
{
    public class Mon_Niv
    {
        public Mon_Niv()
        {
            N_id = -1;
        }

        public Mon_Niv(int m_n_id, int monID, int nivID)
        {
            N_id = m_n_id;
            MonID = monID;
            NivID = nivID;
        }

        public int N_id { get; set; }

        public int MonID { get; set; }

        public int NivID { get; set; }
    }
}