﻿using System;

namespace DrakisReptys.Models
{
    public class Mensaje
    {
        public Mensaje()
        {
            MensajeID = -1;
        }

        public Mensaje(int mensajeID, int monID, int socID, string texto, DateTime fecha)
        {
            MensajeID = mensajeID;
            MonID = monID;
            SocID = socID;
            Texto = texto;
            Fecha = fecha;
        }

        public int MensajeID { get; set; }

        public int MonID { get; set; }

        public int SocID { get; set; }

        public string Texto { get; set; }

        public DateTime Fecha { get; set; }
    }
}