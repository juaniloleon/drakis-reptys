﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys.monitors.crud
{
    public partial class panel : Page
    {
        private int idUser;
        private bool isMonitor;
        private Monitor mon;
        private TipoAcceso priv;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["existeSesion"] == null || Session["isMonitor"] == null || Session["idUser"] == null)
            {
                Response.Redirect("/error/errorPage.aspx");
            }
            else
            {
                bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
                int.TryParse(Session["idUser"].ToString(), out idUser);
                if (!isMonitor)
                {
                    Response.Redirect("/error/errorPage.aspx");
                }
                else
                {
                    mon = LNyAD.ObtenMonitor(idUser);
                    priv = LNyAD.ObtenPrivilegios(mon.TipoAccesoID);
                    lbSaludo.Text = "Bienvenido " + mon.Nombre;
                    if (IsPostBack)
                    {
                        lbMensajeEnviado.Visible = false;
                    }
                    else
                    {
                        var lNiveles = new List<string>();
                        ddlNivelesPuntear.Items.Add("SELECCIONE UNO");
                        ddlNivelMensaje.Items.Add("SELECCIONE UNO");
                        if (priv.NivelAcceso > 5) //Admin
                        {
                            lNiveles = LNyAD.TodosLosNivelesActivos();

                            foreach (var s in lNiveles)
                            {
                                ddlNivelesPuntear.Items.Add(s);
                                ddlNivelMensaje.Items.Add(s);
                            }

                            if (lNiveles.Count > 1)
                            {
                                ddlNivelesPuntear.Items.Add("TODOS MIS NIVELES");
                                ddlNivelMensaje.Items.Add("TODOS MIS NIVELES");
                            }


                            ddlNivelesPuntear.SelectedIndex = 0;
                            ddlNivelMensaje.SelectedIndex = 0;
                        }
                        else
                        {
                            lNiveles = LNyAD.MisNivelesActivos(idUser);
                            foreach (var s in lNiveles)
                            {
                                ddlNivelesPuntear.Items.Add(s);
                                ddlNivelMensaje.Items.Add(s);
                            }

                            btnAdminLevels.Visible = false; //opciones solo admin
                            btnAdminMonitor.Visible = false; //
                            btnInspeccionMensajes.Visible = false;
                            btnPuntos.Visible = false;

                            if (priv.NivelAcceso < 5) //opciones monitor y admin 
                                // Monitor solo consulta y añade (a 0)
                            {
                                //Submonitor no puede administrar nada, solo poner puntos
                                btnAdminActivities.Visible = false;
                                lbAdministrar.Visible = false;
                            }

                            //todo el mundo tiene acceso a miembros, Admin y monitor completo, submonitor de consulta.
                            //btnAdminMembers.Visible = false;
                        }
                    }
                }
            }
        }

        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ddlNivelesPuntear.SelectedIndex == 0) args.IsValid = !true;
        }

        protected void customMessL_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (tbMensaje.Text.Length < 1 || tbMensaje.Text.Length > 75)
                args.IsValid = false;
        }

        protected void btnEnviarMensaje_Click(object sender, EventArgs e)
        {
            Page.Validate("qMess");
            if (Page.IsValid)
            {
                LNyAD.EnviarMensajeANivel(idUser, LNyAD.NivelPorNombre(ddlNivelMensaje.SelectedValue, true),
                    tbMensaje.Text);
                lbMensajeEnviado.Visible = true;
                tbMensaje.Text = "";
            }
        }

        protected void btnPuntearAcceso_Click(object sender, EventArgs e)
        {
            Page.Validate("Puntear");
            if (Page.IsValid)
            {
                Session.Add("idNivelQP", LNyAD.NivelPorNombre(ddlNivelesPuntear.SelectedValue, true));
                Response.Redirect("quickPoints.aspx");
            }
        }

        protected void customLvlMess_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ddlNivelMensaje.SelectedIndex == 0) args.IsValid = !true;
        }

        protected void btnDatos_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserData.aspx");
        }

        protected void btnAdminMembers_Click(object sender, EventArgs e)
        {
            Response.Redirect("crud/membersView.aspx");
        }

        protected void btnVerMensaje_Click(object sender, EventArgs e)
        {
            Response.Redirect("messages.aspx");
        }

        protected void btnAdminMonitor_Click(object sender, EventArgs e)
        {
            Response.Redirect("crud/monitorsView.aspx");
        }

        protected void btnAdminLevels_Click(object sender, EventArgs e)
        {
            Response.Redirect("crud/levelsView.aspx");
        }

        protected void btnInspeccionMensajes_Click(object sender, EventArgs e)
        {
            Response.Redirect("crud/allMessages.aspx");
        }

        protected void btnAdminActivities_Click(object sender, EventArgs e)
        {
            Response.Redirect("crud/activitiesView.aspx");
        }

        protected void btnPuntos_Click(object sender, EventArgs e)
        {
            Response.Redirect("crud/pointsView.aspx");
        }
    }
}