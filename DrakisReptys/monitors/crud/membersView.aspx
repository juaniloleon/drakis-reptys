﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Styles/template.Master" AutoEventWireup="true" CodeBehind="membersView.aspx.cs" Inherits="DrakisReptys.monitors.membersView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row tm-content-row">
            <div class="col-12 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5">
                    <h2 class="tm-block-title">ADMINISTRAR SOCIOS</h2>
                    <br/>
                    <div class="row">
                        <asp:button id="btnVolver" cssclass="btn btn-danger" runat="server" text="VOLVER" onclick="btnVolver_Click"/>
                        <asp:button id="btnNuevo" cssclass="btn btn-warning mx-2" runat="server" text="NUEVO" onclick="btnNew_Click"/>

                        <asp:dropdownlist id="ddlBusqueda" cssclass="mx-2 btn btn-dark" runat="server" autopostback="True" width="25%"></asp:dropdownlist>
                        <asp:dropdownlist id="ddlEstado" cssclass="mx-2 btn btn-dark" runat="server" autopostback="True" width="25%"></asp:dropdownlist>


                        <button onclick="CallPrint()" class="btn btn-warning ">⎙</button>

                    </div>

                    <br/>
                    <asp:label id="lbAviso" runat="server" backcolor="White" font-bold="True" font-italic="True" font-size="Medium" forecolor="Black"></asp:label>
                    <br/>

                    <asp:panel id="pnlToPrint" runat="server">
                        <div class="row justify-content-center" style="overflow: auto">
                            <asp:GridView ID="gvDatos" runat="server" OnRowEditing="gvDatos_RowEditing" OnRowDeleting="gvDatos_RowDeleting">
                                <Columns>
                                    <asp:CommandField ButtonType="Button" ControlStyle-CssClass="btn btn-dark" CausesValidation="False" InsertVisible="False" ShowCancelButton="False" ShowDeleteButton="True" ShowEditButton="True" DeleteText="✘" EditText="✎" ControlStyle-Width="20%">
                                        <ControlStyle CssClass="btn btn-dark"></ControlStyle>
                                    </asp:CommandField>
                                </Columns>
                                <HeaderStyle BorderStyle="None" HorizontalAlign="Center"/>
                                <RowStyle BackColor="#374B5B" HorizontalAlign="Center" Width="100%" Wrap="False"/>
                                <AlternatingRowStyle BackColor="#5B7D99" BorderColor="#435C70" BorderWidth="0px"/>
                            </asp:GridView>
                            <asp:Label ID="lbAvisoVacio" runat="server" Text="NO HAY REGISTROS QUE MOSTRAR"></asp:Label>
                        </div>
                    </asp:panel>
                    <br/>

                    <div class="row">
                        <asp:button id="btnVolver1" cssclass="btn btn-danger" runat="server" text="VOLVER" onclick="btnVolver_Click"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function CallPrint() {
            var printContent = document.getElementById('<%= pnlToPrint.ClientID %>');
            var printWindow = window.open("", "Print Panel", 'left=10000000000,top=10000000000,width=0,height=0');
            printWindow.document.write(printContent.innerHTML);
            printWindow.document.close();
            printWindow.focus();
            printWindow.print();
            printWindow.close();
        }
    </script>
</asp:Content>