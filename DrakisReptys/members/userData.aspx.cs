﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys.members
{
    public partial class userData : Page
    {
        private int idUsuario;
        private bool isMonitor;
        private Socio socio;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["existeSesion"] == null || Session["idUser"] == null || Session["isMonitor"] == null)
            {
                Response.Redirect("/error/errorPage.aspx");
            }
            else
            {
                bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
                if (isMonitor)
                {
                    Response.Redirect("/monitors/panel.aspx");
                }
                else
                {
                    int.TryParse(Session["idUser"].ToString(), out idUsuario);
                    socio = LNyAD.ObtenerSocio(idUsuario);

                    if (socio.Estado == 0)
                    {
                        Response.Redirect("/Default.aspx");
                    }
                    else
                    {
                        if (IsPostBack)
                        {
                        }
                        else
                        {
                            CargaDDLActividad();
                            CargarNivel();

                            tbNombre.Text = socio.Nombre;
                            tbUser.Text = socio.Usuario;
                            tbMail.Text = socio.Correo;
                            tbPass.Text = socio.Clave;
                            tbPass.Attributes["type"] = "Password";

                            tbFecha.Text = socio.FechaNacimiento.ToShortDateString();
                        }
                    }
                }
            }
        }

        private void CargaDDLActividad()
        {
            var actividades = LNyAD.TodasLasActividades();

            foreach (var s in actividades)
            {
                ddlActividad1.Items.Add(s);
                ddlActividad2.Items.Add(s);
            }

            ddlActividad1.SelectedValue = LNyAD.NombreActividad(socio.ActID1);
            ddlActividad2.SelectedValue = LNyAD.NombreActividad(socio.ActID2);
        }

        private void CargarNivel()
        {
            ddlNivel.Items.Add(LNyAD.NombreNivel(socio.NivelID));
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("panel.aspx");
        }

        protected void btnGuardar_click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                socio.Clave = tbPass.Text;
                socio.ActID1 = LNyAD.ActividadPorNombre(ddlActividad1.SelectedValue).Value;
                socio.ActID2 = LNyAD.ActividadPorNombre(ddlActividad2.SelectedValue).Value;
                socio.Correo = tbMail.Text;

                LNyAD.ActualizarAnyadir(socio);
                Response.Redirect("panel.aspx");
            }
        }

        protected void customActividades_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ddlActividad1.SelectedIndex == ddlActividad2.SelectedIndex)
                args.IsValid = false;
        }
    }
}