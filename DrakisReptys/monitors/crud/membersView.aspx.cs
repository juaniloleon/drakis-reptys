﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys.monitors
{
    public partial class membersView : Page
    {
        private int idUser;
        private bool isMonitor;
        private Monitor mon;
        private TipoAcceso priv;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["existeSesion"] == null || Session["isMonitor"] == null || Session["idUser"] == null)
            {
                Response.Redirect("/error/errorPage.aspx");
            }
            else
            {
                bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
                int.TryParse(Session["idUser"].ToString(), out idUser);
                if (!isMonitor)
                {
                    Response.Redirect("/error/errorPage.aspx");
                }
                else
                {
                    mon = LNyAD.ObtenMonitor(idUser);
                    priv = LNyAD.ObtenPrivilegios(mon.TipoAccesoID);
                    if (IsPostBack)
                    {
                        CargarDatos();
                    }
                    else
                    {
                        lbAvisoVacio.Visible = false;

                        CargarDDL();
                        if (priv.NivelAcceso > 5)
                            gvDatos.DataSource = LNyAD.SociosPorNivelTodos(0);
                        else
                            gvDatos.DataSource = LNyAD.MisSocios(idUser);
                        gvDatos.DataBind();

                        if (gvDatos.Rows.Count > 0)
                            AdaptarGV();
                        else
                            lbAvisoVacio.Visible = true;
                    }
                }
            }
        }

        private void CargarDatos()
        {
            if (ddlEstado.SelectedIndex != 2)
            {
                if (ddlBusqueda.SelectedIndex == 0)
                {
                    if (priv.NivelAcceso > 5)
                        gvDatos.DataSource = LNyAD.Socios(ddlEstado.SelectedIndex);
                    else
                        gvDatos.DataSource = LNyAD.MisSocios(idUser, ddlEstado.SelectedIndex);
                }
                else
                {
                    gvDatos.DataSource = LNyAD.SociosPorNivelTodos(LNyAD.NivelPorNombre(ddlBusqueda.SelectedValue),
                        ddlEstado.SelectedIndex);
                }
            }
            else //TODOS LOS ESTADOS
            {
                if (ddlBusqueda.SelectedIndex == 0)
                {
                    if (priv.NivelAcceso > 5)
                        gvDatos.DataSource = LNyAD.Socios();
                    else
                        gvDatos.DataSource = LNyAD.MisSocios(idUser);
                }
                else
                {
                    gvDatos.DataSource = LNyAD.SociosPorNivelTodos(LNyAD.NivelPorNombre(ddlBusqueda.SelectedValue));
                }
            }


            gvDatos.DataBind();
            if (gvDatos.Rows.Count > 0)
            {
                lbAvisoVacio.Visible = false;

                AdaptarGV();
            }
            else
            {
                lbAvisoVacio.Visible = true;
            }
        }

        private void AdaptarGV()
        {
            gvDatos.HeaderRow.Cells[1].Visible = false;
            gvDatos.HeaderRow.Cells[6].Visible = false;
            gvDatos.HeaderRow.Cells[2].Text = "Nivel";
            gvDatos.HeaderRow.Cells[8].Text = "Actividad 1";
            gvDatos.HeaderRow.Cells[9].Text = "Actividad 2";


            if (ddlBusqueda.SelectedIndex != 0)
                gvDatos.HeaderRow.Cells[2].Visible = false;
            else
                gvDatos.HeaderRow.Cells[2].Visible = true;

            if (priv.NivelAcceso < 5)
                gvDatos.HeaderRow.Cells[0].Visible = false; //No tiene permisos para editar o borrar


            foreach (GridViewRow r in gvDatos.Rows)
            {
                r.Cells[1].Visible = false;
                r.Cells[6].Visible = false;

                r.Cells[8].Text = LNyAD.NombreActividad(r.Cells[8].Text);
                r.Cells[9].Text = LNyAD.NombreActividad(r.Cells[9].Text);
                r.Cells[10].Text = r.Cells[10].Text.Split(' ')[0];

                if (ddlBusqueda.SelectedIndex == 0)
                    r.Cells[2].Text = LNyAD.NombreNivel(r.Cells[2].Text);
                else
                    r.Cells[2].Visible = false;

                r.Cells[7].Text = r.Cells[7].Text.Equals("1") ? "Activo" : "Baja";

                if (priv.NivelAcceso < 5) r.Cells[0].Visible = false;
            }
        }

        private void CargarDDL()
        {
            var niveles = new List<string>();

            niveles.Add("TODOS LOS NIVELES");

            //ddlBusqueda.Items
            if (priv.NivelAcceso > 5) //Admin controla todos
                niveles.AddRange(LNyAD.TodosLosNiveles());
            else
                niveles.AddRange(LNyAD.MisNivelesActivos(idUser));

            foreach (var s in niveles) ddlBusqueda.Items.Add(s);

            ddlBusqueda.SelectedIndex = 0;

            var estados = new List<string> {"BAJA", "ACTIVOS", "TODOS LOS ESTADOS"};
            foreach (var s in estados) ddlEstado.Items.Add(s);
            ddlEstado.SelectedIndex = 2;
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("/monitors/panel.aspx");
        }


        protected void gvDatos_RowEditing(object sender, GridViewEditEventArgs e)
        {
            Session.Add("idEditUser", gvDatos.Rows[e.NewEditIndex].Cells[1].Text);
            Session.Add("TypeEditUserMonitor", false);
            Response.Redirect("editUser.aspx");
        }

        protected void gvDatos_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var soc = LNyAD.ObtenerSocio(gvDatos.Rows[e.RowIndex].Cells[1].Text);
            if (soc.Estado == 0)
            {
                LNyAD.BorrarSocio(gvDatos.Rows[e.RowIndex].Cells[1].Text);
                lbAviso.Text = "Borrado -> " + soc.Nombre;
            }
            else
            {
                lbAviso.Text = "No se puede borrar -> " + soc.Nombre + ". Primero debe estar en estado de baja.";
            }

            CargarDatos();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Session.Add("idEditUser", -1);
            Session.Add("TypeEditUserMonitor", false);
            Response.Redirect("editUser.aspx");
        }
    }
}