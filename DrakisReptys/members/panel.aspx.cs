﻿using System;
using System.Web.UI;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys.members
{
    public partial class portal : Page
    {
        private int idUsuario;
        private bool isMonitor;
        private Socio socio;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["existeSesion"] == null || Session["idUser"] == null || Session["isMonitor"] == null)
            {
                Response.Redirect("/error/errorPage.aspx");
            }
            else
            {
                bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
                if (isMonitor)
                {
                    Response.Redirect("/monitors/panel.aspx");
                }
                else
                {
                    int.TryParse(Session["idUser"].ToString(), out idUsuario);
                    socio = LNyAD.ObtenerSocio(idUsuario);

                    if (socio.Estado == 0) Response.Redirect("/Default.aspx");


                    lbAct1.Text = LNyAD.NombreActividad(socio.ActID1);
                    lbAct2.Text = LNyAD.NombreActividad(socio.ActID2);

                    lbUserName.Text = socio.Nombre;

                    lbPuntos.Text = "Tienes un total de: " + LNyAD.TotalPuntos(idUsuario);

                    CargarMovimientosPuntos();
                    lbZero.Visible = gvPuntos.Rows.Count < 1;
                }
            }
        }

        private void CargarMovimientosPuntos()
        {
            gvPuntos.DataSource = LNyAD.MovimientoPorSocio(idUsuario);
            gvPuntos.DataBind();

            if (gvPuntos.Rows.Count != 0)
            {
                gvPuntos.HeaderRow.Cells[0].Visible = false;
                gvPuntos.HeaderRow.Cells[1].Visible = false;
                gvPuntos.HeaderRow.Cells[2].Visible = false;

                for (var i = 0; i < gvPuntos.Rows.Count; i++)
                {
                    gvPuntos.Rows[i].Cells[0].Visible = false;
                    gvPuntos.Rows[i].Cells[1].Visible = false;
                    gvPuntos.Rows[i].Cells[2].Visible = false;

                    gvPuntos.Rows[i].Cells[4].Text = LNyAD.FormatoEuropeoFecha(gvPuntos.Rows[i].Cells[4].Text); ;
                }
            }
        }

        protected void btnData_Click(object sender, EventArgs e)
        {
            Response.Redirect("userData.aspx");
        }

        protected void btnMessage_Click(object sender, EventArgs e)
        {
            Response.Redirect("messages.aspx");
        }
    }
}