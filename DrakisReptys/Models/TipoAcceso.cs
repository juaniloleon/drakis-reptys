﻿using DrakisReptys.Logic;

namespace DrakisReptys.Models
{
    public class TipoAcceso
    {
        public TipoAcceso(ReptysDataSet.TipoAccesoRow tipoAccesoRow)
        {
            TipoAccesoID = tipoAccesoRow.ID;
            NivelAcceso = tipoAccesoRow.NivelAcceso;
            Nombre1 = tipoAccesoRow.Nombre;
        }

        //Desde la IU no se podrá crear un nuevo elemento de este modelo.
        public TipoAcceso(int tipoAccesoID, int nivelAcceso, string nombre)
        {
            TipoAccesoID = tipoAccesoID;
            NivelAcceso = nivelAcceso;
            Nombre1 = nombre;
        }

        public int TipoAccesoID { get; set; }

        public int NivelAcceso { get; set; }

        public string Nombre1 { get; set; }
    }
}