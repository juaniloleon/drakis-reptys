﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrakisReptys.Logic;

namespace DrakisReptys.control
{
    public partial class recover : Page
    {
        private int? idRecovery;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
            }
            else
            {
                CargarDDLMiembros();
            }
        }


        private void CargarDDLMiembros()
        {
            ddlMonitor.Items.Add("Seleccione uno");
            ddlMonitor.Items.Add("Monitor");
            ddlMonitor.Items.Add("Socio");
            ddlMonitor.SelectedIndex = 0;
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            if (Session["idRecovery"] != null)
                Session.Remove("idRecovery");
            Response.Redirect("/control/login.aspx");
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            Validate();
            if (Page.IsValid)
            {
                Session.Add("idRecovery", idRecovery);
                Session.Add("isMonitor", ddlMonitor.SelectedIndex == 1 ? true : false);

                Response.Redirect("recover2.aspx");
            }
        }


        protected void CustomSelect_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ddlMonitor.SelectedIndex == 0)
                args.IsValid = false;
        }

        protected void Custom_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (null == (idRecovery =
                    LNyAD.UserMailValido(tbUser.Text, tbMail.Text, ddlMonitor.SelectedIndex == 1 ? true : false)))
                args.IsValid = false;
        }
    }
}