﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys.monitors.crud
{
    public partial class userData : Page
    {
        private int idUser;
        private bool isMonitor;
        private Monitor mon;
        private TipoAcceso priv;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["existeSesion"] == null || Session["isMonitor"] == null || Session["idUser"] == null)
            {
                Response.Redirect("/error/errorPage.aspx");
            }
            else
            {
                bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
                int.TryParse(Session["idUser"].ToString(), out idUser);
                if (!isMonitor)
                {
                    Response.Redirect("/error/errorPage.aspx");
                }
                else
                {
                    mon = LNyAD.ObtenMonitor(idUser);
                    priv = LNyAD.ObtenPrivilegios(mon.TipoAccesoID);
                    if (IsPostBack)
                    {
                    }
                    else
                    {
                        CargarDatos(priv.NivelAcceso);
                    }
                }
            }
        }

        private void CargarDatos(int nivelAcceso)
        {
            tbNombre.Text = mon.Nombre;
            tbUser.Text = mon.Usuario;
            tbTelefono.Text = mon.Telefono;
            tbPregunta.Text = mon.Pregunta;
            tbPass.Attributes["type"] = "Password";
            tbRespuesta.Text = mon.Respuesta;
            tbMail.Text = mon.Correo;
            tbPass.Text = mon.Clave;

            if (nivelAcceso < 10)
            {
                tbUser.Enabled = false;
                tbUser.CssClass = "form-control text-dark";
            }
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("panel.aspx");
        }

        protected void btnGuardar_click(object sender, EventArgs e)
        {
            Validate();
            if (Page.IsValid)
            {
                mon.Nombre = tbNombre.Text;
                mon.Pregunta = tbPregunta.Text;
                mon.Respuesta = tbRespuesta.Text;
                mon.Usuario = tbUser.Text;
                mon.Clave = tbPass.Text;
                mon.Correo = tbMail.Text;
                mon.Telefono = tbTelefono.Text;

                LNyAD.ActualizarAnyadir(mon);
                Response.Redirect("panel.aspx");
            }
        }

        protected void customUser_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (!LNyAD.MonitorLibre(idUser, tbUser.Text)) args.IsValid = false;
        }

        protected void customMail(object source, ServerValidateEventArgs args)
        {
            if (!LNyAD.MonitorMailLibre(idUser, tbMail.Text)) args.IsValid = false;
        }


        protected void customPhoneF(object source, ServerValidateEventArgs args)
        {
            if (!LNyAD.FormaTelefono(tbTelefono.Text)) args.IsValid = false;
        }
    }
}