﻿using System;
using System.Web.UI;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys.members
{
    public partial class messages : Page
    {
        private int idUsuario;
        private bool isMonitor;
        private Socio socio;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["existeSesion"] == null || Session["idUser"] == null || Session["isMonitor"] == null)
            {
                Response.Redirect("/error/errorPage.aspx");
            }
            else
            {
                bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
                if (isMonitor)
                {
                    Response.Redirect("/monitors/panel.aspx");
                }
                else
                {
                    int.TryParse(Session["idUser"].ToString(), out idUsuario);
                    socio = LNyAD.ObtenerSocio(idUsuario);

                    if (socio.Estado == 0)
                    {
                        Response.Redirect("/Default.aspx");
                    }
                    else
                    {
                        gvMensajes.DataSource = LNyAD.Mensajes(idUsuario, false);
                        gvMensajes.DataBind();

                        if (gvMensajes.Rows.Count > 0)
                        {
                            lbZero.Visible = false;

                            gvMensajes.HeaderRow.Cells[0].Visible = false;
                            gvMensajes.HeaderRow.Cells[2].Visible = false;

                            gvMensajes.HeaderRow.Cells[3].Visible = false;

                            for (var i = 0; i < gvMensajes.Rows.Count; i++)
                            {
                                gvMensajes.Rows[i].Cells[0].Visible = false;
                                gvMensajes.Rows[i].Cells[2].Visible = false;
                                gvMensajes.Rows[i].Cells[4].Text = LNyAD.FormatoEuropeoFecha(gvMensajes.Rows[i].Cells[4].Text);

                                gvMensajes.Rows[i].Cells[3].Visible = false;
                            }
                        }
                        else
                        {
                            lbZero.Visible = true;
                        }
                    }
                }
            }
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("/members/panel.aspx");
        }
    }
}