﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Styles/template.Master" AutoEventWireup="true" CodeBehind="quickPoints.aspx.cs" Inherits="DrakisReptys.monitors.crud.quickPoints" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row tm-content-row">
            <div class="col-sm-12 col-md-12 col-lg-8 col-xl-8 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5">
                    <h2 class="tm-block-title">PuntViar</h2>
                    <div class="row justify-content-center" style="overflow: auto">
                        <asp:Button ID="btnVovler" runat="server" CssClass="btn btn-block btn-danger" Text="VOLVER" OnClick="btnVovler_Click2"/>

                        <asp:GridView ID="gvQuickPoints" runat="server">
                            <Columns>
                                <asp:BoundField HeaderText="Total" ControlStyle-CssClass="d-none"/>
                                <asp:BoundField ShowHeader="False" ItemStyle-Width="65%"/>
                            </Columns>
                        </asp:GridView>
                        <asp:Label ID="lbGVvacio" runat="server" Text="NO HAY DATOS QUE MOSTRAR" Visible="False"></asp:Label>
                        <asp:Button ID="btnVolver2" runat="server" CssClass="btn btn-block btn-danger" Text="VOLVER" OnClick="btnVolver2_Click"/>

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>