﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Styles/template.Master" AutoEventWireup="true" CodeBehind="editUser.aspx.cs" Inherits="DrakisReptys.monitors.crud.editUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row tm-content-row">
            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5">
                    <h2 class="tm-block-title">FICHA</h2>
                    <div class="row">
                        <asp:Button ID="btnVolver" CssClass="btn btn-danger" runat="server" Text="VOLVER" OnClick="btnVolver_Click" CausesValidation="False"/>
                    </div><br/>

                    <div class="row">
                        <asp:Label ID="lbNombre" runat="server" CssClass="text-light" Text="Nombre y Apellidos:"></asp:Label>
                        <asp:RequiredFieldValidator ID="Req" runat="server" ErrorMessage="* EL CAMPO ES OBLIGATORIO *" Font-Bold="True" ForeColor="Black" ControlToValidate="tbNombre" ValidationGroup="Generico"></asp:RequiredFieldValidator>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="tbNombre" CssClass="form-control " runat="server" MaxLength="60"></asp:TextBox>

                    </div>
                    <div class="row">
                        <asp:Label ID="lbUser" runat="server" CssClass="text-light" Text="Usuario"></asp:Label>
                        <asp:RequiredFieldValidator ID="Req0" runat="server" ErrorMessage="* EL CAMPO ES OBLIGATORIO *" Font-Bold="True" ForeColor="Black" ControlToValidate="tbUser" ValidationGroup="Generico"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="customUser" runat="server" ControlToValidate="tbUser" ErrorMessage="*EL USUARIO YA ESTÁ ASIGNADO*" Font-Bold="True" ForeColor="Black" OnServerValidate="customUser_ServerValidate" ValidationGroup="Generico"></asp:CustomValidator>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="tbUser" CssClass="form-control " runat="server" MaxLength="8"></asp:TextBox>

                    </div>
                    <div class="row">
                        <asp:Label ID="lbPass" runat="server" CssClass="text-light" Text="Contraseña"></asp:Label>
                    </div>
                    <div class="row">
                        <asp:Button ID="btnNewPass" CssClass="btn btn-outline-warning" runat="server" Text="↺ RESETEAR CLAVE" OnClick="btnNewPass_Click"/>

                        <asp:Label ID="lbAviso" runat="server" CssClass="text-light" Text=""></asp:Label>

                    </div>
                    <div class="row">
                    </div>
                    <div class="row">

                    </div>
                    <div class="row">
                        <asp:Label ID="lbMail" runat="server" CssClass="text-light" Text="Correo"></asp:Label>
                        <asp:RequiredFieldValidator ID="Req3" runat="server" ControlToValidate="tbMail" Display="Dynamic" ErrorMessage="* EL CAMPO ES OBLIGATORIO *" Font-Bold="True" ForeColor="Black" ValidationGroup="Generico"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="CustomMonitorMail" runat="server" ControlToValidate="tbMail" Display="Dynamic" ErrorMessage="*ESTE MAIL YA ESTÁ ASIGNADO A UN MONITOR*" Font-Bold="True" ForeColor="Black" ValidationGroup="vgMon" OnServerValidate="CustomMonitorMail_ServerValidate"></asp:CustomValidator>
                        <asp:RegularExpressionValidator ID="regExMail" runat="server" ControlToValidate="tbMail" Display="Dynamic" ErrorMessage="*FORMATO NO VÁLIDO*" Font-Bold="True" ForeColor="Black" ValidationGroup="Generico" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="tbMail" CssClass="form-control " runat="server" MaxLength="100"></asp:TextBox>

                    </div>
                    <br/>
                    <div class="row">
                        <asp:Label ID="lbTipoNivel" runat="server" CssClass="text-light" Text="TipoONivel" Visible="False"></asp:Label>
                        <asp:CustomValidator ID="CustomTipoNivel" runat="server" ControlToValidate="ddlTipo" Display="Dynamic" ErrorMessage="*SELECCIONE UNA OPCIÓN*" Font-Bold="True" ForeColor="Black" ValidationGroup="vgMon" OnServerValidate="CustomTipo_ServerValidate"></asp:CustomValidator>
                        <asp:CustomValidator ID="CustomNivel" runat="server" ControlToValidate="ddlNivel" Display="Dynamic" ErrorMessage="*SELECCIONE UNA OPCIÓN*" Font-Bold="True" ForeColor="Black" ValidationGroup="vgSoc" OnServerValidate="CustomNivel_ServerValidate"></asp:CustomValidator>
                    </div>
                    <div class="row">
                        <asp:DropDownList ID="ddlNivel" runat="server" CssClass="btn-block btn btn-outline-light" Visible="False"></asp:DropDownList>
                        <asp:DropDownList ID="ddlTipo" runat="server" CssClass="btn-block btn btn-outline-light" Visible="False"></asp:DropDownList>

                    </div>
                    <br/>
                    <div class="row">
                        <asp:Label ID="lbTelefonoActividad" runat="server" CssClass="text-light" Text="TelefonoOActividad" Visible="False"></asp:Label>
                        <asp:CustomValidator ID="cvActividades" runat="server" ControlToValidate="ddlActividad2" ErrorMessage="*DEBE SELECCIONAR 2 ACTIVIDADES DIFERENTES*" Font-Bold="True" ForeColor="Black" ValidationGroup="vgSoc" OnServerValidate="cvActividades_ServerValidate"></asp:CustomValidator>
                        <asp:RequiredFieldValidator ID="reqTlf" runat="server" ControlToValidate="tbTelefono" ErrorMessage="*EL CAMPO ES OBLIGATORIO*" Font-Bold="True" ForeColor="Black" ValidationGroup="vgMon"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="cvTlfno" runat="server" ControlToValidate="tbTelefono" Display="Dynamic" ErrorMessage="*EL TELEFONO NO CUMPLE EL FORMATO*" Font-Bold="True" ForeColor="Black" ValidationGroup="vgMon" OnServerValidate="cvTlfno_ServerValidate"></asp:CustomValidator>
                    </div>
                    <div class="row">
                        <asp:DropDownList ID="ddlActividad1" runat="server" CssClass="btn-block btn btn-outline-light" Visible="False"></asp:DropDownList>
                        <asp:DropDownList ID="ddlActividad2" runat="server" CssClass="btn-block btn btn-outline-light" Visible="False"></asp:DropDownList>
                        <asp:TextBox ID="tbTelefono" CssClass="form-control " runat="server" Visible="False"></asp:TextBox>

                    </div>
                    <br/>
                    <div class="row">
                        <asp:Label ID="lbPregunta" runat="server" CssClass="text-light" Text="Pregunta de Seguridad" Visible="False"></asp:Label>
                        <asp:RequiredFieldValidator ID="reqPregunta" runat="server" ControlToValidate="tbPregunta" ErrorMessage="*EL CAMPO ES OBLIGATORIO*" Font-Bold="True" ForeColor="Black" ValidationGroup="vgMon"></asp:RequiredFieldValidator>
                    </div>
                    <div class="row">
                        <asp:Label ID="lbFecha" runat="server" CssClass="text-light" Text="Cumpleaños:" Visible="False"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tbFecha" ErrorMessage="*EL CAMPO ES OBLIGATORIO*" Font-Bold="True" ForeColor="Black" ValidationGroup="vgSoc"></asp:RequiredFieldValidator>
                    </div>
                    <div class="row">
                        <asp:CustomValidator ID="CustomDateType" runat="server" ErrorMessage="*FORMATO NO VÁLIDO DD/MM/AAAA*" Font-Bold="True" ForeColor="Black" OnServerValidate="CustomDateType_ServerValidate" ControlToValidate="tbFecha" ValidationGroup="vgSoc">*FORMATO NO VÁLIDO DD/MM/AAAA*</asp:CustomValidator>
                        <asp:TextBox ID="tbPregunta" CssClass="form-control " runat="server" Visible="False" MaxLength="50"></asp:TextBox>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="tbFecha" CssClass="form-control " runat="server" Visible="False"></asp:TextBox>
                    </div>
                    <div class="row">
                        <asp:Label ID="lbRespuesta" runat="server" CssClass="text-light" Text="Respuesta de Seguridad" Visible="False"></asp:Label>
                        <asp:RequiredFieldValidator ID="reqRespuesta" runat="server" ControlToValidate="tbRespuesta" ErrorMessage="*EL CAMPO ES OBLIGATORIO*" Font-Bold="True" ForeColor="Black" ValidationGroup="vgMon"></asp:RequiredFieldValidator>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="tbRespuesta" CssClass="form-control " runat="server" Visible="False" MaxLength="50"></asp:TextBox>

                    </div>
                    <div class="row">
                        <asp:Label ID="lbActivo" runat="server" Text="Activo"></asp:Label>
                        <asp:RadioButton ID="cbActivo" runat="server" CssClass="mx-1" GroupName="acti"/>
                        <asp:Label ID="lbBaja" runat="server" Text="Baja"></asp:Label>
                        <asp:RadioButton ID="cbBaja" runat="server" CssClass="mx-1" GroupName="acti"/>
                        <asp:CustomValidator ID="CVeSTADO" runat="server" ControlToValidate="tbRespuesta" ErrorMessage="*SELECCIONE UNA OPCIÓN*" Font-Bold="True" ForeColor="Black" OnServerValidate="CVeSTADO_ServerValidate" ValidationGroup="Generico"></asp:CustomValidator>
                    </div>
                    <br/>
                    <div class="row">
                        <asp:Button ID="btnVolver1" CssClass="btn btn-danger" runat="server" Text="VOLVER" OnClick="btnVolver_Click" Width="50%" CausesValidation="False"/>
                        <asp:Button ID="btnGuardar" CssClass="btn btn-success" runat="server" Text="Guardar" OnClick="btnGuardar_click" Visible="False" Width="50%" CausesValidation="False"/>

                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>