﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys.monitors.crud
{
    public partial class activitiesView : Page
    {
        private int idUser;
        private bool isMonitor;
        private Monitor mon;
        private TipoAcceso priv;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["existeSesion"] == null || Session["isMonitor"] == null || Session["idUser"] == null)
            {
                Response.Redirect("/error/errorPage.aspx");
            }
            else
            {
                bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
                int.TryParse(Session["idUser"].ToString(), out idUser);

                if (!isMonitor)
                {
                    Response.Redirect("/error/errorPage.aspx");
                }
                else
                {
                    mon = LNyAD.ObtenMonitor(idUser);
                    priv = LNyAD.ObtenPrivilegios(mon.TipoAccesoID);
                    if(priv.NivelAcceso<5) Response.Redirect("/error/errorPage.aspx");
                    if (IsPostBack)
                    {
                        CargarDatos();
                    }
                    else
                    {
                        CargarDatos();

                        lbAvisoVacio.Visible = false;


                        if (gvDatos.Rows.Count > 0)
                            AdaptarGV();
                        else
                            lbAvisoVacio.Visible = true;
                    }
                }
            }
        }

        private void AdaptarGV()
        {
            gvDatos.HeaderRow.Cells[1].Visible = false;
            gvDatos.HeaderRow.Cells[2].Text = "MONITOR";

            gvDatos.HeaderRow.Cells[4].Visible = false;

            foreach (GridViewRow r in gvDatos.Rows)
            {
                r.Cells[1].Visible = false;
                r.Cells[2].Text = LNyAD.ObtenMonitor(Convert.ToInt32(r.Cells[2].Text)).Nombre;
                r.Cells[4].Visible = false;
            }
        }

        private void CargarDatos()
        {
            gvDatos.DataSource = LNyAD.Actividades();
            gvDatos.DataBind();
        }

        protected void gvDatos_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (gvDatos.Rows.Count > 2)
            {
                LNyAD.BorrarActividad(gvDatos.Rows[e.RowIndex].Cells[1].Text);
                e.Cancel = true;
                lbAviso.Text = "BORRADO";
                lbAviso.Visible = true;
                Response.Redirect("activitiesView.aspx");
            }
            else
            {
                lbAviso.Visible = true;
                lbAviso.Text = "SE NECESITAN 2 ACTIVIDADES COMO MÍNIMO";
            }
        }

        protected void gvDatos_RowEditing(object sender, GridViewEditEventArgs e)
        {
            Session.Add("idActivity", gvDatos.Rows[e.NewEditIndex].Cells[1].Text);
            Response.Redirect("editActivity.aspx");
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Session.Add("idActivity", -1);
            Response.Redirect("editActivity.aspx");
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("/monitors/panel.aspx");
        }
    }
}