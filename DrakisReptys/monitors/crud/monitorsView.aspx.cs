﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys.monitors.crud
{
    public partial class monitorsView : Page
    {
        private int idUser;
        private bool isMonitor;
        private Monitor mon;
        private TipoAcceso priv;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["existeSesion"] == null || Session["isMonitor"] == null || Session["idUser"] == null)
            {
                Response.Redirect("/error/errorPage.aspx");
            }
            else
            {
                bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
                int.TryParse(Session["idUser"].ToString(), out idUser);
                mon = LNyAD.ObtenMonitor(idUser);
                priv = LNyAD.ObtenPrivilegios(mon.TipoAccesoID);
                if (!isMonitor || priv.NivelAcceso < 10)
                {
                    Response.Redirect("/error/errorPage.aspx");
                }

                else
                {
                    mon = LNyAD.ObtenMonitor(idUser);
                    priv = LNyAD.ObtenPrivilegios(mon.TipoAccesoID);
                    if (IsPostBack)
                    {
                        CargarDatos();
                    }
                    else
                    {
                        lbAvisoVacio.Visible = false;

                        CargarDDL();
                        gvDatos.DataSource = LNyAD.Monitores(0);
                        gvDatos.DataBind();


                        if (gvDatos.Rows.Count > 0)
                            AdaptarGV();
                        else
                            lbAvisoVacio.Visible = true;
                    }
                }
            }
        }

        private void CargarDDL()
        {
            var tipos = new List<string>();

            tipos.Add("TODOS LOS TIPOS");


            tipos.AddRange(LNyAD.TiposMonitor(100));


            foreach (var s in tipos) ddlBusqueda.Items.Add(s);

            ddlBusqueda.SelectedIndex = 0;
        }

        private void CargarDatos()
        {
            if (ddlBusqueda.SelectedIndex == 0)
                gvDatos.DataSource = LNyAD.Monitores();
            else
                gvDatos.DataSource = LNyAD.Monitores(LNyAD.TipoAccesoPorNombre(ddlBusqueda.SelectedValue));
            gvDatos.DataBind();
            AdaptarGV();
        }

        private void AdaptarGV()
        {
            gvDatos.HeaderRow.Cells[1].Visible = false;
            gvDatos.HeaderRow.Cells[5].Visible = false;
            gvDatos.HeaderRow.Cells[8].Visible = false;
            gvDatos.HeaderRow.Cells[9].Visible = false;
            gvDatos.HeaderRow.Cells[2].Text = "TIPO";

            if (ddlBusqueda.SelectedIndex != 0)
                gvDatos.HeaderRow.Cells[2].Visible = false;
            else
                gvDatos.HeaderRow.Cells[2].Visible = true;

            foreach (GridViewRow r in gvDatos.Rows)
            {
                r.Cells[1].Visible = false;
                r.Cells[5].Visible = false;
                r.Cells[8].Visible = false;
                r.Cells[9].Visible = false;
                if (ddlBusqueda.SelectedIndex == 0)
                    r.Cells[2].Text = LNyAD.NombreAcceso(r.Cells[2].Text);
                else
                    r.Cells[2].Visible = false;

                r.Cells[10].Text = r.Cells[10].Text.Equals("1") ? "Activo" : "Baja";
            }
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("/monitors/panel.aspx");
        }


        protected void gvDatos_RowEditing(object sender, GridViewEditEventArgs e)
        {
            Session.Add("idEditUser", gvDatos.Rows[e.NewEditIndex].Cells[1].Text);
            Session.Add("TypeEditUserMonitor", true);
            Response.Redirect("editUser.aspx");
        }

        protected void gvDatos_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var mon = LNyAD.ObtenMonitor(gvDatos.Rows[e.RowIndex].Cells[1].Text);

            if (mon.MonitorID == idUser)
            {
                lbAviso.Text = "No te puedes eliminar a ti mismo  " + mon.Nombre + ". Usa otro administrador.";
            }
            else if (mon.Estado == 0)
            {
                LNyAD.BorrarMonitor(gvDatos.Rows[e.RowIndex].Cells[1].Text);
                lbAviso.Text = "Borrado -> " + mon.Nombre;
            }

            else
            {
                lbAviso.Text = "No se puede borrar -> " + mon.Nombre + ". Primero debe estar en estado de baja.";
            }

            e.Cancel = true;
            CargarDatos();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Session.Add("idEditUser", -1); //#004
            Session.Add("TypeEditUserMonitor", true);
            Response.Redirect("editUser.aspx");
        }

        protected void btnRelations_Click(object sender, EventArgs e)
        {
            Response.Redirect("linkMonitorLevel.aspx");
        }
    }
}