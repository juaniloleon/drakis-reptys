﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys.monitors.crud
{
    public partial class editUser : Page
    {
        private bool EditIsMonitor;
        private int idEditUser;

        private int idUser;
        private bool isMonitor;
        private Monitor mon;

        private Monitor monEdit;
        private TipoAcceso priv;
        private Socio socEdit;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["idEditUser"] == null || Session["TypeEditUserMonitor"] == null ||
                Session["existeSesion"] == null || Session["isMonitor"] == null || Session["idUser"] == null)
            {
                Response.Redirect("/error/errorPage.aspx");
            }
            else
            {
                bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
                int.TryParse(Session["idUser"].ToString(), out idUser);
                bool.TryParse(Session["TypeEditUserMonitor"].ToString(), out EditIsMonitor);
                int.TryParse(Session["idEditUser"].ToString(), out idEditUser);
                if (!isMonitor)
                {
                    LimpiarSesion();

                    Response.Redirect("/error/errorPage.aspx");
                }
                else
                {
                    mon = LNyAD.ObtenMonitor(idUser);
                    priv = LNyAD.ObtenPrivilegios(mon.TipoAccesoID);

                    if (Page.IsPostBack)
                    {
                    }
                    else
                    {
                        if (idEditUser == idUser)
                        {
                            ddlTipo.Enabled = false;
                            cbActivo.Enabled = false;
                            cbBaja.Enabled = false;
                        }

                        tbFecha.Text = "DD/MM/AAAA";
                        CargarDDLTipos();
                        CargarDDLNiveles();
                        CargarDDLActividad();

                        ddlTipo.Visible = false;
                        ddlNivel.Visible = false;
                        OcultarModificados();

                        if (EditIsMonitor) //monitor
                        {
                            FormularioMonitor();
                            if (idEditUser < 0)
                            {
                                monEdit = new Monitor();
                            }
                            else
                            {
                                monEdit = LNyAD.ObtenMonitor(idEditUser);

                                tbNombre.Text = monEdit.Nombre;
                                tbMail.Text = monEdit.Correo;
                                tbPregunta.Text = monEdit.Pregunta;
                                tbRespuesta.Text = monEdit.Respuesta;
                                tbTelefono.Text = monEdit.Telefono;
                                ddlTipo.SelectedValue = LNyAD.NombreAcceso(monEdit.TipoAccesoID);
                                if (monEdit.Estado == 1)
                                    cbActivo.Checked = true;
                                else
                                    cbBaja.Checked = true;
                            }

                            tbUser.Text = monEdit.Usuario;
                        }
                        else //socios
                        {
                            FormularioSocio();

                            if (idEditUser < 0)
                            {
                                socEdit = new Socio();
                                cbBaja.Checked = true;
                                lbAviso.Text = "Se le enviará una clave al usuario";
                            }
                            else
                            {
                                socEdit = LNyAD.ObtenerSocio(idEditUser);

                                tbNombre.Text = socEdit.Nombre;
                                tbMail.Text = socEdit.Correo;
                                tbUser.Text = socEdit.Usuario;
                                ddlNivel.SelectedValue = LNyAD.NombreNivel(socEdit.NivelID);
                                ddlActividad1.SelectedValue = LNyAD.NombreActividad(socEdit.ActID1);
                                ddlActividad2.SelectedValue = LNyAD.NombreActividad(socEdit.ActID2);
                                tbFecha.Text = socEdit.FechaNacimiento.ToShortDateString();
                                //tbFecha.Attributes["type"] = "Date";

                                if (socEdit.Estado == 1)
                                    cbActivo.Checked = true;
                                else
                                    cbBaja.Checked = true;
                            }
                        }

                        btnGuardar.Visible = true;
                        if (idEditUser < 0) btnNewPass.Enabled = false;
                    }
                }
            }
        }

        private void OcultarModificados()
        {
            ddlTipo.Visible = false;
            ddlNivel.Visible = false;

            lbTipoNivel.Visible = false;
            ddlActividad1.Visible = false;
            ddlActividad2.Visible = false;
            lbTelefonoActividad.Visible = false;
            tbTelefono.Visible = false;
            lbPregunta.Visible = false;
            lbRespuesta.Visible = false;
            tbPregunta.Visible = false;
            tbRespuesta.Visible = false;
            btnGuardar.Visible = false;

            lbFecha.Visible = false;
            tbFecha.Visible = false;
        }


        //Adapta el formulario
        private void FormularioSocio()
        {
            ddlNivel.Visible = true;
            ddlActividad1.Visible = true;
            ddlActividad2.Visible = true;

            lbTelefonoActividad.Visible = true;
            lbTelefonoActividad.Text = "Actividades:";
            lbFecha.Visible = true;
            tbFecha.Visible = true;
        }

        //Carga las actividades existentes
        private void CargarDDLActividad()
        {
            ddlActividad1.Items.Add("Seleccione actividad 1");
            ddlActividad2.Items.Add("Seleccione actividad 2");

            var lista = LNyAD.TodasLasActividades();
            foreach (var s in lista)
            {
                ddlActividad1.Items.Add(s);
                ddlActividad2.Items.Add(s);
            }

            ddlActividad1.SelectedIndex = 0;
            ddlActividad2.SelectedIndex = 0;
        }


        //Adapta el formulario
        private void FormularioMonitor()
        {
            ddlTipo.Visible = true;
            tbTelefono.Visible = true;
            lbTelefonoActividad.Visible = true;
            lbTelefonoActividad.Text = "Teléfono:";
            lbPregunta.Visible = true;
            lbRespuesta.Visible = true;
            tbPregunta.Visible = true;
            tbRespuesta.Visible = true;
            tbPregunta.Text = LNyAD.PreguntaSeguridad();

            lbTipoNivel.Visible = true;
            lbTipoNivel.Text = "Tipo:";
        }

        //Carga los niveles existentes
        private void CargarDDLNiveles()
        {
            ddlNivel.Items.Add("Seleccione nivel");
            var lista = LNyAD.TodosLosNivelesActivos();
            foreach (var s in lista)
                ddlNivel.Items.Add(s);
            ddlNivel.SelectedIndex = 0;
            ddlNivel.Visible = true;
        }

        //Carga los tipos de monitor existente
        private void CargarDDLTipos()
        {
            ddlTipo.Items.Add("Seleccione tipo");
            var lista = LNyAD.TiposMonitor(100);
            foreach (var s in lista)
                ddlTipo.Items.Add(s);
            ddlTipo.SelectedIndex = 0;
            ddlTipo.Visible = true;
            //lbTipoNivel.Visible = true;
            //lbTipoNivel.Text = "Modo:";
        }


        protected void cvActividades_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ddlActividad2.SelectedIndex == ddlActividad1.SelectedIndex || ddlActividad1.SelectedIndex == 0 ||
                ddlActividad2.SelectedIndex == 0) args.IsValid = false;
        }

        protected void cvTlfno_ServerValidate(object source, ServerValidateEventArgs args)
        {
            int tlf;
            int.TryParse(tbTelefono.Text, out tlf);
            if (tlf > 999999999 || tlf < 600000000) args.IsValid = false;
        }

        protected void CustomTipo_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ddlTipo.SelectedIndex == 0) args.IsValid = false;
        }

        protected void CustomNivel_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ddlNivel.SelectedIndex == 0) args.IsValid = false;
        }

        protected void CustomMonitorMail_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (LNyAD.ExisteMailMonitor(tbMail.Text, idEditUser)) args.IsValid = false;
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            LimpiarSesion();

            Response.Redirect(EditIsMonitor ? "/monitors/crud/monitorsView.aspx" : "/monitors/crud/membersView.aspx");
        }

        protected void btnGuardar_click(object sender, EventArgs e)
        {
            Validate("Generico"); //Campos comunes

            if (Page.IsValid)
            {
                if (EditIsMonitor) //editar monitor
                {
                    Validate("vgMon"); //Campos exclusivos
                    if (Page.IsValid)
                    {
                        monEdit = idEditUser < 0 ? new Monitor() : LNyAD.ObtenMonitor(idEditUser);

                        monEdit.Correo = tbMail.Text;
                        monEdit.Estado = cbActivo.Checked ? 1 : 0;
                        monEdit.Nombre = tbNombre.Text;
                        monEdit.Pregunta = tbPregunta.Text;
                        monEdit.Respuesta = tbRespuesta.Text;
                        monEdit.Telefono = tbTelefono.Text;
                        monEdit.TipoAccesoID = LNyAD.TipoAccesoPorNombre(ddlTipo.SelectedValue);
                        monEdit.Usuario = tbUser.Text;

                        if (idEditUser < 0)
                            monEdit.Clave = LNyAD.GenerarClave();

                        LNyAD.ActualizarAnyadir(monEdit);

                        if (idEditUser < 0)
                            LNyAD.CambiarClave(true, LNyAD.MonitorByMail(monEdit.Correo).Value);
                    }
                }
                else //editar socio
                {
                    Validate("vgSoc"); //Campos exclusivos
                    if (Page.IsValid)
                    {
                        socEdit = idEditUser < 0 ? new Socio() : LNyAD.ObtenerSocio(idEditUser);

                        socEdit.Correo = tbMail.Text;
                        socEdit.Estado = cbActivo.Checked ? 1 : 0;
                        socEdit.Nombre = tbNombre.Text;
                        socEdit.FechaNacimiento = DateTime.Parse(tbFecha.Text).Date;
                        socEdit.NivelID = LNyAD.NivelPorNombre(ddlNivel.SelectedValue);
                        socEdit.Usuario = tbUser.Text;
                        socEdit.ActID1 = LNyAD.ActividadPorNombre(ddlActividad1.SelectedValue).Value;
                        socEdit.ActID2 = LNyAD.ActividadPorNombre(ddlActividad2.SelectedValue).Value;
                        if (idEditUser < 0)
                            socEdit.Clave = LNyAD.GenerarClave();
                        LNyAD.ActualizarAnyadir(socEdit);
                        if (idEditUser < 0)
                            LNyAD.CambiarClave(false, LNyAD.SocioPorUser(socEdit.Usuario, socEdit.Correo).Value);
                    }
                }
            }

            if (!Page.IsValid) return;
            LimpiarSesion();
            Response.Redirect(
                EditIsMonitor ? "/monitors/crud/monitorsView.aspx" : "/monitors/crud/membersView.aspx");
        }


        protected void customUser_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = LNyAD.UserLibrePropio(tbUser.Text, EditIsMonitor, idEditUser);
        }

        protected void btnNewPass_Click(object sender, EventArgs e)
        {
            LNyAD.CambiarClave(EditIsMonitor, idEditUser);

            LimpiarSesion();

            Response.Redirect("/monitors/crud/membersView.aspx");
        }

        private void LimpiarSesion()
        {
            Session.Remove("TypeEditUserMonitor");
            Session.Remove("idEditUser");
        }

        protected void CustomDateType_ServerValidate(object source, ServerValidateEventArgs args)
        {
            var aux = tbFecha.Text.Split('/');


            if (aux.Length != 3)
            {
                args.IsValid = false;
            }
            else
            {
                int.TryParse(aux[0], out var dia);
                int.TryParse(aux[1], out var mes);
                int.TryParse(aux[2], out var anyo);

                if (anyo < 1000 || anyo > 9999 || mes > 12 || mes < 1 || dia < 1 || dia > 31)
                    args.IsValid = false;
            }
        }

        protected void CVeSTADO_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (!cbActivo.Checked && !cbBaja.Checked)
                args.IsValid = false;
        }
    }
}