﻿using DrakisReptys.Logic;

namespace DrakisReptys.Models
{
    public class Monitor
    {
        public Monitor() //Contructor vacio
        {
            MonitorID = -1;
        }

        public Monitor(ReptysDataSet.MonitoresRow monitoresRow)
        {
            MonitorID = monitoresRow.ID;
            TipoAccesoID = monitoresRow.TipoAccesoID;
            Estado = monitoresRow.Estado;
            Nombre = monitoresRow.Nombre;
            Usuario = monitoresRow.Usuario;
            Clave = monitoresRow.Clave;
            Correo = monitoresRow.Correo;
            Telefono = monitoresRow.Telefono;
            Pregunta = monitoresRow.Pregunta;
            Respuesta = monitoresRow.Respuesta;
        }

        public Monitor(int monitorID, int tipoAccesoID, int estado, string nombre, string usuario, string clave,
            string correo, string telefono, string pregunta, string respuesta)
        {
            MonitorID = monitorID;
            TipoAccesoID = tipoAccesoID;
            Estado = estado;
            Nombre = nombre;
            Usuario = usuario;
            Clave = clave;
            Correo = correo;
            Telefono = telefono;
            Pregunta = pregunta;
            Respuesta = respuesta;
        }

        public int MonitorID { get; set; }

        public int TipoAccesoID { get; set; }

        public int Estado { get; set; }

        public string Nombre { get; set; }

        public string Usuario { get; set; }

        public string Clave { get; set; }

        public string Correo { get; set; }

        public string Telefono { get; set; }

        public string Pregunta { get; set; }

        public string Respuesta { get; set; }
    }
}