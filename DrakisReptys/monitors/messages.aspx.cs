﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys.monitors.crud
{
    public partial class messages : Page
    {
        private int idUser;
        private bool isMonitor;
        private Monitor mon;
        private TipoAcceso priv;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["existeSesion"] == null || Session["idUser"] == null || Session["isMonitor"] == null)
            {
                Response.Redirect("/error/errorPage.aspx");
            }
            else
            {
                bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
                int.TryParse(Session["idUser"].ToString(), out idUser);
                mon = LNyAD.ObtenMonitor(idUser);
                priv = LNyAD.ObtenPrivilegios(mon.TipoAccesoID);
                if (!isMonitor)
                {
                    Response.Redirect("/monitors/panel.aspx");
                }
                else
                {
                    if (mon.Estado == 0)
                    {
                        Response.Redirect("/Default.aspx");
                    }
                    else
                    {
                        gvMensajes.DataSource = LNyAD.Mensajes(idUser, true);
                        gvMensajes.DataBind();


                        if (gvMensajes.Rows.Count > 0)
                        {
                            lbAvisoVacio.Visible = false;

                            gvMensajes.HeaderRow.Cells[1].Visible = false;
                            gvMensajes.HeaderRow.Cells[4].Visible = false;

                            gvMensajes.HeaderRow.Cells[3].Visible = false;

                            for (var i = 0; i < gvMensajes.Rows.Count; i++)
                            {
                                gvMensajes.Rows[i].Cells[1].Visible = false;
                                gvMensajes.Rows[i].Cells[4].Visible = false;
                                gvMensajes.Rows[i].Cells[5].Text = LNyAD.FormatoEuropeoFecha(gvMensajes.Rows[i].Cells[5].Text);

                                gvMensajes.Rows[i].Cells[3].Visible = false;
                            }
                        }
                        else
                        {
                            lbAvisoVacio.Visible = true;
                        }
                    }
                }
            }
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("/members/panel.aspx");
        }

        protected void gvMensajes_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            LNyAD.BorrarMensaje(gvMensajes.Rows[e.RowIndex].Cells[1].Text);
            Response.Redirect("messages.aspx");
        }
    }
}