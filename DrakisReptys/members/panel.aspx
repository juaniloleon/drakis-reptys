﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Styles/template.Master" AutoEventWireup="true" CodeBehind="panel.aspx.cs" Inherits="DrakisReptys.members.portal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row tm-content-row">
            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5">
                    <h2 class="tm-block-title">TUS PUNTOS</h2>
                    <div class="row align-content-center">
                    </div>
                    <div class="row align-content-center justify-content-center align-items-center" style="overflow: auto">
                        <asp:GridView ID="gvPuntos" runat="server">
                            <HeaderStyle BorderStyle="None" HorizontalAlign="Center"/>
                            <RowStyle BackColor="#374B5B" HorizontalAlign="Center" Width="100%" Wrap="False"/>
                            <AlternatingRowStyle BackColor="#5B7D99" BorderColor="#435C70" BorderWidth="0px"/>
                        </asp:GridView>

                        <asp:Label ID="lbZero" runat="server" Text="AÚN NO TIENES MOVIMIENTO DE PUNTOS"></asp:Label>
                    </div>


                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5">
                    <h2 class="tm-block-title">Bienvenido <asp:Label ID="lbUserName" runat="server" Text="USER"></asp:Label></h2>
                    <div class="row align-content-center justify-content-center align-items-center">
                        <asp:Label ID="lbPuntos" runat="server" Text="Label" CssClass="alert alert-light ml-3 "></asp:Label>
                    </div>
                    <hr/>
                    <h2 class="tm-block-title">TUS ACTIVIDADES</h2>

                    <div class="row align-content-center justify-content-center align-items-center">
                        <asp:Label ID="lbAct1" runat="server" Text="Label" CssClass="alert alert-light" Width="45%"></asp:Label>
                        <asp:Label ID="lbAct2" runat="server" Text="Label" CssClass="alert alert-light ml-3" Width="45%"></asp:Label>
                    </div>
                    <hr/>
                    <div class="row">
                        <asp:Button ID="btnData" runat="server" Text="Ver datos" CssClass="btn btn-warning" Width="50%" CausesValidation="False" OnClick="btnData_Click"/>
                        <asp:Button ID="btnMessage" runat="server" Text="Mensajes" CssClass="btn btn-success" Width="50%" CausesValidation="False" OnClick="btnMessage_Click"/>


                    </div>


                </div>
            </div>
        </div>
    </div>
</asp:Content>