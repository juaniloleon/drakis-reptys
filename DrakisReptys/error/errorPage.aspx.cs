﻿using System;
using System.Web.UI;

namespace DrakisReptys.error
{
    public partial class errorPage : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.RemoveAll();
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Default.aspx");
        }
    }
}