﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Styles/template.Master" AutoEventWireup="true" CodeBehind="userData.aspx.cs" Inherits="DrakisReptys.monitors.crud.userData" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row tm-content-row">
            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col ">
                <div class="tm-bg-primary-dark text-light p-5">
                    <h2 class="tm-block-title">DATOS</h2>
                    <div class="row">
                        <asp:Button ID="btnVolver" CssClass="btn btn-danger" runat="server" Text="VOLVER" OnClick="btnVolver_Click" CausesValidation="False"/>
                    </div><br/>

                    <div class="row">
                        <asp:Label ID="lbNombre" runat="server" CssClass="text-light" Text="Nombre y Apellidos:"></asp:Label>
                        <asp:RequiredFieldValidator ID="Req" runat="server" ErrorMessage="* EL CAMPO ES OBLIGATORIO *" Font-Bold="True" ForeColor="Black" ControlToValidate="tbNombre" ValidationGroup="Generico"></asp:RequiredFieldValidator>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="tbNombre" CssClass="form-control" runat="server" MaxLength="60"></asp:TextBox>

                    </div>
                    <div class="row">
                        <asp:Label ID="lbUser" runat="server" CssClass="text-light" Text="Usuario"></asp:Label>
                        <asp:RequiredFieldValidator ID="Req4" runat="server" ErrorMessage="* EL CAMPO ES OBLIGATORIO *" Font-Bold="True" ForeColor="Black" ControlToValidate="tbUser" ValidationGroup="Generico"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="customUser" runat="server" ControlToValidate="tbUser" ErrorMessage="*ESTE USUARIO YA EXISTE*" Font-Bold="True" ForeColor="Black" OnServerValidate="customUser_ServerValidate" ValidationGroup="Generico"></asp:CustomValidator>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="tbUser" CssClass="form-control" runat="server" MaxLength="8"></asp:TextBox>

                    </div>
                    <div class="row">
                        <asp:Label ID="lbPass" runat="server" CssClass="text-light" Text="Contraseña"></asp:Label>
                        <asp:RequiredFieldValidator ID="Req5" runat="server" ErrorMessage="* EL CAMPO ES OBLIGATORIO *" Font-Bold="True" ForeColor="Black" ControlToValidate="tbPass" ValidationGroup="Generico"></asp:RequiredFieldValidator>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="tbPass" CssClass="form-control " runat="server" MaxLength="20"></asp:TextBox>

                    </div>
                    <div class="row">
                        <asp:Label ID="lbPassR" runat="server" CssClass="text-light" Text="Repita la Contraseña:"></asp:Label>
                        <asp:RequiredFieldValidator ID="Req2" runat="server" ControlToValidate="tbPassR" ErrorMessage="* EL CAMPO ES OBLIGATORIO *" Font-Bold="True" ForeColor="Black" ValidationGroup="Generico"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidatorPass" runat="server" ControlToCompare="tbPass" ControlToValidate="tbPassR" ErrorMessage="*LAS CONTRASEÑAS NO COINCIDEN*" Font-Bold="True" ForeColor="Black" ValidationGroup="Generico"></asp:CompareValidator>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="tbPassR" CssClass="form-control " runat="server" TextMode="Password" MaxLength="20"></asp:TextBox>

                    </div>
                    <div class="row">
                        <asp:Label ID="lbMail" runat="server" CssClass="text-light" Text="Correo: "></asp:Label>
                        <asp:RequiredFieldValidator ID="Req3" runat="server" ControlToValidate="tbMail" ErrorMessage="* EL CAMPO ES OBLIGATORIO *" Font-Bold="True" ForeColor="Black" ValidationGroup="Generico"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="regexMail" runat="server" ControlToValidate="tbMail" ErrorMessage="*EL FORMATO DEL EMAIL NO ES VÁLIDO*" Font-Bold="True" ForeColor="Black" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        <asp:CustomValidator ID="customUser0" runat="server" ControlToValidate="tbUser" ErrorMessage="*ESTE MAIL YA EXISTE*" Font-Bold="True" ForeColor="Black" OnServerValidate="customMail" ValidationGroup="Generico"></asp:CustomValidator>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="tbMail" CssClass="form-control " runat="server" MaxLength="100"></asp:TextBox>

                    </div>
                    <div class="row">
                        <asp:Label ID="lbTelefono" runat="server" CssClass="text-light" Text="Teléfono"></asp:Label>
                        <asp:RequiredFieldValidator ID="Req6" runat="server" ErrorMessage="* EL CAMPO ES OBLIGATORIO *" Font-Bold="True" ForeColor="Black" ControlToValidate="tbTelefono" ValidationGroup="Generico"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="cPhoneFormato" runat="server" ControlToValidate="tbTelefono" ErrorMessage="*EL FORMATO DEL TELÉFONO NO ES VÁLIDO*" Font-Bold="True" ForeColor="Black" OnServerValidate="customPhoneF" ValidationGroup="Generico"></asp:CustomValidator>
                    </div>

                    <div class="row">
                        <asp:TextBox ID="tbTelefono" CssClass="form-control " runat="server" TextMode="SingleLine" MaxLength="9"></asp:TextBox>
                    </div>
                    <div class="row">
                        <asp:Label ID="lbPregunta" runat="server" CssClass="text-light" Text="Pregunta"></asp:Label>
                        <asp:RequiredFieldValidator ID="Req7" runat="server" ErrorMessage="* EL CAMPO ES OBLIGATORIO *" Font-Bold="True" ForeColor="Black" ControlToValidate="tbPregunta" ValidationGroup="Generico"></asp:RequiredFieldValidator>
                    </div>

                    <div class="row">
                        <asp:TextBox ID="tbPregunta" CssClass="form-control" runat="server" MaxLength="50"></asp:TextBox>
                    </div>
                    <div class="row">
                        <asp:Label ID="blRespuesta" runat="server" CssClass="text-light" Text="Respuesta"></asp:Label>
                        <asp:RequiredFieldValidator ID="Req8" runat="server" ErrorMessage="* EL CAMPO ES OBLIGATORIO *" Font-Bold="True" ForeColor="Black" ControlToValidate="tbRespuesta" ValidationGroup="Generico"></asp:RequiredFieldValidator>
                    </div>

                    <div class="row">
                        <asp:TextBox ID="tbRespuesta" CssClass="form-control" runat="server" MaxLength="50"></asp:TextBox>
                    </div>

                    <br/>
                    <div class="row">
                        <asp:Button ID="btnVolver1" CssClass="btn btn-danger" runat="server" Text="VOLVER" OnClick="btnVolver_Click" Width="50%" CausesValidation="False"/>
                        <asp:Button ID="btnGuardar" CssClass="btn btn-success" runat="server" Text="GUARDAR" OnClick="btnGuardar_click" Width="50%" CausesValidation="False"/>

                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>