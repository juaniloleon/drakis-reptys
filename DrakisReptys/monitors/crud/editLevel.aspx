﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Styles/template.Master" AutoEventWireup="true" CodeBehind="editLevel.aspx.cs" Inherits="DrakisReptys.monitors.crud.editLevel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row tm-content-row">
            <div class="col-sm-12 col-md-12 col-lg-8 col-xl-8 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5">
                    <h2 class="tm-block-title">FICHA NIVEL</h2>
                    <div class="row">
                        <asp:Button ID="btnVolver" CssClass="btn btn-danger" runat="server" Text="VOLVER" OnClick="btnVolver_Click"/>
                    </div>

                    <div class="row">
                        NOMBRE:
                        <asp:CustomValidator ID="cvNombre" runat="server" ControlToValidate="tbNombre" ErrorMessage="*YA HAY UN NIVEL CON ESE NOMBRE*" Font-Bold="True" ForeColor="Black" OnServerValidate="cvNombre_ServerValidate"></asp:CustomValidator>
                        <asp:RequiredFieldValidator ID="reqnombre" runat="server" ControlToValidate="tbNombre" ErrorMessage="*EL CAMPO ES OBLIGATORIO*" Font-Bold="True" ForeColor="Black"></asp:RequiredFieldValidator>
                    </div>
                    <div class="row">
                        <asp:TextBox ID="tbNombre" CssClass="form-control" runat="server" MaxLength="15"></asp:TextBox>
                    </div>
                    <br/>
                    <div class="row">
                        <asp:DropDownList ID="ddlEsatado" CssClass="btn btn-outline-light" runat="server"></asp:DropDownList>
                        <asp:CustomValidator ID="cvEstado" runat="server" ControlToValidate="ddlEsatado" ErrorMessage="*SELECCIONE UNA OPCIÓN*" Font-Bold="True" ForeColor="Black" OnServerValidate="cvEstado_ServerValidate"></asp:CustomValidator>
                    </div>
                    <div class="row">
                        <asp:Button ID="btnCancelar" CssClass="btn btn-danger" runat="server" Text="CANCELAR" OnClick="btnVolver_Click"/>
                        <asp:Button ID="btnGuardar" CssClass="btn btn-success mx-2" runat="server" Text="GUARDAR" OnClick="btnGuardar_Click"/>

                    </div>


                </div>
            </div>
        </div>
    </div>
</asp:Content>