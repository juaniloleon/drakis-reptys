﻿using DrakisReptys.Logic;

namespace DrakisReptys.Models
{
    public class Actividad
    {
        public Actividad()
        {
            ActividadID = -1;
        }

        public Actividad(ReptysDataSet.ActividadesRow r)
        {
            ActividadID = r.ID;
            MonitorID = r.MonitorID;
            Nombre = r.Nombre;
        }

        public Actividad(int actividadID, int monitorID, string nombre)
        {
            ActividadID = actividadID;
            MonitorID = monitorID;
            Nombre = nombre;
        }

        public int ActividadID { get; set; }

        public int MonitorID { get; set; }

        public string Nombre { get; set; }
    }
}