﻿using System;
using System.Web.UI;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys
{
    public partial class Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
            }
            else
            {
                CargarDDLNiveles();
                CargarDGVActividades();
                CargarDGVCumpleaños();
            }

            DataGVRanking();
            if (gvRanking.Rows.Count > 0)
            {
                AdaptarGVRanking();
                lbVacio.Visible = false;
            }
            else
            {
                lbVacio.Visible = true;
            }
        }


        private void CargarDGVCumpleaños()
        {
            var cumples = LNyAD.Cumpleanyos();
            lbCumpleList.Text = "";
            foreach (var s in cumples) lbCumpleList.Text += s + "<br/>";
        }

        private void CargarDGVActividades()
        {
            var actividades = LNyAD.MostrarActividades();

            lbActividadsGV.Text = "";
            foreach (var s in actividades) lbActividadsGV.Text += s;
        }

        private void AdaptarGVRanking() //Oculta y modifica los campos
        {
            gvRanking.HeaderRow.Cells[3].Visible = false;
            gvRanking.HeaderRow.Cells[4].Visible = false;
            gvRanking.HeaderRow.Cells[5].Visible = false;
            gvRanking.HeaderRow.Cells[6].Visible = false;
            gvRanking.HeaderRow.Cells[7].Visible = false;

            for (var i = 0; i < gvRanking.Rows.Count; i++)
            {
                int.TryParse(gvRanking.Rows[i].Cells[5].Text, out var idAux);
                var socio = LNyAD.ObtenerSocio(idAux);


                gvRanking.Rows[i].Cells[3].Visible = false;
                gvRanking.Rows[i].Cells[4].Visible = false;
                gvRanking.Rows[i].Cells[5].Visible = false;
                gvRanking.Rows[i].Cells[6].Visible = false;
                gvRanking.Rows[i].Cells[7].Visible = false;

                gvRanking.Rows[i].Cells[0].Text = (i + 1).ToString();
                gvRanking.Rows[i].Cells[1].Text = socio.Nombre;
                gvRanking.Rows[i].Cells[2].Text = LNyAD.NombreNivel(socio.NivelID);
            }
        }

        private void DataGVRanking()
        {
            gvRanking.DataSource = ddlNivel.SelectedIndex == 0 ? LNyAD.RankingTotal() : LNyAD.RankingNivel(LNyAD.NivelPorNombre(ddlNivel.SelectedValue));
            DataBind();
        }

        private void CargarDDLNiveles()
        {
            var niveles = LNyAD.TodosLosNivelesActivos();
            ddlNivel.Items.Add("Todos los niveles");

            foreach (var s in niveles) ddlNivel.Items.Add(s);
            ddlNivel.SelectedIndex = 0;
        }

       
    }
}