﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys.monitors.crud
{
    public partial class editLevel : Page
    {
        private int  idEditLevel;
        private bool isMonitor;
        private Nivel level;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["idEditLevel"] == null || Session["existeSesion"] == null || Session["isMonitor"] == null ||
                Session["idUser"] == null)
            {
                Response.Redirect("/error/errorPage.aspx");
            }
            else
            {
                bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
                int.TryParse(Session["idEditLevel"].ToString(), out idEditLevel);
                if (!isMonitor)
                {
                    LimpiarSesion();

                    Response.Redirect("/error/errorPage.aspx");
                }
                else
                {
                    if (Page.IsPostBack)
                    {
                    }
                    else
                    {
                        ddlEsatado.Items.Add("DESHABILITADO");
                        ddlEsatado.Items.Add("ACTIVO");
                        ddlEsatado.Items.Add("SELECCIONE UNA OPCIÓN");


                        if (idEditLevel > 0)
                        {
                            level = LNyAD.ObtenNivel(idEditLevel);
                            tbNombre.Text = level.Nombre;
                            ddlEsatado.SelectedIndex = level.Estado;
                        }
                        else
                        {
                            level = new Nivel();
                            ddlEsatado.SelectedIndex = 2;
                        }
                    }
                }
            }
        }

        private void LimpiarSesion()
        {
            Session.Remove("idEditLevel");
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("/monitors/CRUD/levelsView.aspx");
        }

        protected void cvEstado_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (ddlEsatado.SelectedIndex == 2)
                args.IsValid = false;
        }

        protected void cvNombre_ServerValidate(object source, ServerValidateEventArgs args)
        {
            var aux = LNyAD.ExisteNombreNivel(tbNombre.Text);

            if (aux != null)
                if (aux != idEditLevel)
                    args.IsValid = false;
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            Validate();

            if (!Page.IsValid) return;
            Nivel level;
            level = idEditLevel < 0 ? new Nivel() : LNyAD.ObtenNivel(idEditLevel);
                
            level.Nombre = tbNombre.Text;
            level.Estado = ddlEsatado.SelectedIndex;

            LNyAD.ActualizarAnyadir(level);
            Response.Redirect("/monitors/CRUD/levelsView.aspx");
        }
    }
}