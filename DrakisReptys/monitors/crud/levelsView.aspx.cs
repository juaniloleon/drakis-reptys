﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using DrakisReptys.Logic;
using DrakisReptys.Models;

namespace DrakisReptys.monitors.crud
{
    public partial class levelsView : Page
    {
        private int idUser;
        private bool isMonitor;
        private Monitor mon;
        private TipoAcceso priv;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["existeSesion"] == null || Session["isMonitor"] == null || Session["idUser"] == null)
            {
                Response.Redirect("/error/errorPage.aspx");
            }
            else
            {
                bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
                int.TryParse(Session["idUser"].ToString(), out idUser);
                mon = LNyAD.ObtenMonitor(idUser);
                priv = LNyAD.ObtenPrivilegios(mon.TipoAccesoID);
                if (!isMonitor || priv.NivelAcceso < 10)
                {
                    Response.Redirect("/error/errorPage.aspx");
                }
                else
                {
                    mon = LNyAD.ObtenMonitor(idUser);
                    priv = LNyAD.ObtenPrivilegios(mon.TipoAccesoID);
                    if (IsPostBack)
                    {
                        CargarDatos();
                    }
                    else
                    {
                        lbAvisoVacio.Visible = false;

                        CargarDDL();
                        gvDatos.DataSource = LNyAD.Niveles();
                        gvDatos.DataBind();


                        if (gvDatos.Rows.Count > 0)
                            AdaptarGV();
                        else
                            lbAvisoVacio.Visible = true;
                    }
                }
            }
        }

        private void CargarDatos()
        {
            lbAvisoVacio.Visible = false;

            gvDatos.DataSource = ddlBusqueda.SelectedIndex == 2 ? LNyAD.Niveles() : LNyAD.NivelesEstado(ddlBusqueda.SelectedIndex);

            gvDatos.DataBind();

            if (gvDatos.Rows.Count > 0)
                AdaptarGV();
            else
                lbAvisoVacio.Visible = true;
        }

        private void AdaptarGV()
        {
            gvDatos.HeaderRow.Cells[1].Visible = false;
            foreach (GridViewRow r in gvDatos.Rows)
            {
                r.Cells[1].Visible = false;
                r.Cells[3].Text = r.Cells[3].Text.Equals("0") ? "DESHABILITADO" : "ACTIVO";
            }
        }

        private void CargarDDL()
        {
            ddlBusqueda.Items.Add("DESHABILITADOS");
            ddlBusqueda.Items.Add("ACTIVOS");
            ddlBusqueda.Items.Add("TODOS LOS ESTADOS");

            ddlBusqueda.SelectedIndex = 2;
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("/monitors/panel.aspx");
        }


        protected void btnNew_Click(object sender, EventArgs e)
        {
            Session.Add("idEditLevel", -1);
            Response.Redirect("editLevel.aspx");
        }

        protected void btnRelations_Click(object sender, EventArgs e)
        {
            Response.Redirect("linkMonitorLevel.aspx");
        }

        protected void gvDatos_RowDeleting1(object sender, GridViewDeleteEventArgs e)
        {
            var niv = LNyAD.ObtenNivel(gvDatos.Rows[e.RowIndex].Cells[1].Text);


            if (niv.Estado == 0)
            {
                LNyAD.BorrarNivel(gvDatos.Rows[e.RowIndex].Cells[1].Text);
                lbAviso.Text = "Borrado -> " + niv.Nombre;
            }

            else
            {
                lbAviso.Text = "No se puede borrar -> " + niv.Nombre + ". Primero debe estar en estado deshabilitado.";
            }

            e.Cancel = true;
            CargarDatos();
        }

        protected void gvDatos_RowEditing1(object sender, GridViewEditEventArgs e)
        {
            var aux = gvDatos.Rows[e.NewEditIndex].Cells[1].Text;
            Session.Add("idEditLevel", aux);
            Response.Redirect("editLevel.aspx");
        }
    }
}