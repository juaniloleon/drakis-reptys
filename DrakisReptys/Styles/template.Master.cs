﻿using System;
using System.Web.UI;

namespace DrakisReptys.Styles
{
    public partial class template : MasterPage
    {
        private bool isMonitor;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["existeSesion"] == null)
            {
                btnSalir.Visible = false;
                btnLogin.Visible = true;
            }
            else
            {
                btnSalir.Visible = true;
                btnLogin.Visible = false;
            }

            if (Session["isMonitor"] != null) bool.TryParse(Session["isMonitor"].ToString(), out isMonitor);
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Session.RemoveAll();
            Response.Redirect("/Default.aspx");
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            Response.Redirect("/control/login.aspx");
        }


        protected void btnHome_Click(object sender, EventArgs e)
        {
            if (Session["existeSesion"] == null)
            {
                Response.Redirect("/Default.aspx");
            }
            else
            {
                if (isMonitor)
                    Response.Redirect("/monitors/panel.aspx");

                else
                    Response.Redirect("/members/panel.aspx");
            }
        }
    }
}