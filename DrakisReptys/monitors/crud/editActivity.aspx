﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Styles/template.Master" AutoEventWireup="true" CodeBehind="editActivity.aspx.cs" Inherits="DrakisReptys.monitors.crud.editActivity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="row tm-content-row">
            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
                <div class="tm-bg-primary-dark text-light p-5">
                    <h2 class="tm-block-title">FICHA ACTIVIDAD</h2>
                    <div class="row">

                        <asp:Button ID="btnVolver1" CssClass="btn btn-danger" runat="server" Text="VOLVER" OnClick="btnVolver_Click" CausesValidation="False"/>

                    </div>

                    <div class="row">
                        <asp:DropDownList ID="ddlMonitor" CssClass="btn btn-light" runat="server"></asp:DropDownList>

                        <asp:CustomValidator ID="cvMonitor" runat="server" ErrorMessage="*SELECCIONE UN MONITOR*" ControlToValidate="ddlMonitor" Font-Bold="True" ForeColor="Black" OnServerValidate="cvMonitor_ServerValidate"></asp:CustomValidator>
                        <asp:RequiredFieldValidator ID="reqNombre" runat="server" ErrorMessage="*EL NOMBRE ES OBLIGATORIO*" ControlToValidate="tbNombre" Font-Bold="True" ForeColor="Black"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="cvNombre" runat="server" ErrorMessage="*YA HAY UNA ACTIVIDAD CON ESE NOMBRE*" ControlToValidate="tbNombre" Font-Bold="True" ForeColor="Black" OnServerValidate="cvNombre_ServerValidate"></asp:CustomValidator>
                    </div>
                    <div class="row">
                        NOMBRE:
                        <asp:TextBox ID="tbNombre" CssClass="form-control" runat="server" MaxLength="15"></asp:TextBox>

                    </div>


                    <div class="row">
                        <asp:Button ID="btnVolver" CssClass="btn btn-danger" runat="server" Text="VOLVER" OnClick="btnVolver_Click" CausesValidation="False"/>
                        <asp:Button ID="btnGuardar" CssClass="btn btn-success" runat="server" Text="GUARDAR" OnClick="btnGuardar_Click"/>


                    </div>

                </div>
            </div>
        </div>
    </div>


</asp:Content>